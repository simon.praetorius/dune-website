+++
title = "Dune Developer Meeting 2017 -- Agenda for Parallel Interface in dune-grid"
+++

We did not actually meet, we just collected some agenda points...

# Communication on intersections (indices...)

# Asynchronous Communication
- Return a future from communicate...?
- There is an implementation in ALUGrid

# Intersection/entity subsets for communication
- Allows to do Overlap of communication/computation  
  (e.g. step 1: do pre-computation on the entities that are needed for communication, step 2: do pre+post-computation on all other entities in parallel with communication, step 3: do post-computation on the entities from step 1.)