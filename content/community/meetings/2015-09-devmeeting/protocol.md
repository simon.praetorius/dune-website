Meeting Minutes DUNE Developer Meeting 2015
===========================================

<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Preliminaries</a>
<ul>
<li><a href="#sec-1-1">1.1. Core developer status for Dominic.</a></li>
<li><a href="#sec-1-2">1.2. Discussion time will be appointed for each topic beforehand.</a></li>
<li><a href="#sec-1-3">1.3. Every core developer will be</a></li>
</ul>
</li>
<li><a href="#sec-2">2. Review of 2013 meeting</a>
<ul>
<li><a href="#sec-2-1">2.1. Licensing</a></li>
<li><a href="#sec-2-2">2.2. CMake vs. Autotools</a></li>
<li><a href="#sec-2-3">2.3. Supported Compilers</a></li>
<li><a href="#sec-2-4">2.4. Release is out.</a></li>
<li><a href="#sec-2-5">2.5. QuadratureRuleKey in dune-geometry</a></li>
<li><a href="#sec-2-6">2.6. EntityIterators are true STL forward_iterators.</a></li>
<li><a href="#sec-2-7">2.7. Entity pointer removal.</a></li>
<li><a href="#sec-2-8">2.8. Intersections are now copyable.</a></li>
<li><a href="#sec-2-9">2.9. Gridcheck headers renamed.</a></li>
<li><a href="#sec-2-10">2.10. Need to remove boost checks from dune-common/dune-istl</a></li>
<li><a href="#sec-2-11">2.11. Getting rid of Dune::istl_assign_to_fmatrix</a></li>
<li><a href="#sec-2-12">2.12. Change the template argument list of the geometry</a></li>
<li><a href="#sec-2-13">2.13. Getting rid of the PartitionType template parameter on the GridView</a></li>
<li><a href="#sec-2-14">2.14. Duplicated methods on Grid and GridView</a></li>
<li><a href="#sec-2-15">2.15. geomTypes() on the IndexSet</a></li>
<li><a href="#sec-2-16">2.16. Range based for</a></li>
<li><a href="#sec-2-17">2.17. parallel features of the grid interface</a></li>
<li><a href="#sec-2-18">2.18. Capabilities.</a></li>
<li><a href="#sec-2-19">2.19. intersectionIds</a></li>
<li><a href="#sec-2-20">2.20. hasBoundaryIntersections()</a></li>
</ul>
</li>
<li><a href="#sec-3">3. General</a>
<ul>
<li><a href="#sec-3-1">3.1. <span class="done DONE">DONE</span> Briefly visit 2014 protocol</a></li>
<li><a href="#sec-3-2">3.2. Compiler versions</a></li>
<li><a href="#sec-3-3">3.3. Rework website (CMake compatible replacement for WML)</a></li>
<li><a href="#sec-3-4">3.4. Infrastructure (Should we switch to gitlab? How to organize votes?) -&gt; see below</a></li>
<li><a href="#sec-3-5">3.5. How to implement finite elements with higher order regularity (C1,&#x2026;)?</a></li>
<li><a href="#sec-3-6">3.6. Release paper</a></li>
</ul>
</li>
<li><a href="#sec-4">4. Build system</a>
<ul>
<li><a href="#sec-4-1">4.1. CMake version in the 3.0 release will be 3.0.*</a></li>
<li><a href="#sec-4-2">4.2. Abolition of the magic that builds test during "make test"</a></li>
<li><a href="#sec-4-3">4.3. Parallel tests</a></li>
<li><a href="#sec-4-4">4.4. Documentation of the CMake build system will be written by Dominic!</a></li>
<li><a href="#sec-4-5">4.5. Use named arguments in CMake.</a></li>
<li><a href="#sec-4-6">4.6. Remove enable trick</a></li>
<li><a href="#sec-4-7">4.7. Alberta worldim will set at configure time.</a></li>
</ul>
</li>
<li><a href="#sec-5">5. Request for new features</a>
<ul>
<li><a href="#sec-5-1">5.1. Introduce namespaces for 3.0</a></li>
<li><a href="#sec-5-2">5.2. Remove imported std classes from DUNE namespace.</a></li>
</ul>
</li>
<li><a href="#sec-6">6. Clarification of interface</a>
<ul>
<li><a href="#sec-6-1">6.1. Do pre- and postAdapt always have to be called?</a></li>
<li><a href="#sec-6-2">6.2. GridFactory: Can one insert elements before all vertices are inserted?</a></li>
<li><a href="#sec-6-3">6.3. viewThreadSafe: What guarantees does it provide?</a></li>
</ul>
</li>
<li><a href="#sec-7">7. Change requests for interfaces</a>
<ul>
<li><a href="#sec-7-1">7.1. Functional subroutines</a>
<ul>
<li><a href="#sec-7-1-1">7.1.1. quadrature rules</a></li>
<li><a href="#sec-7-1-2">7.1.2. Reference Elements.</a></li>
<li><a href="#sec-7-1-3">7.1.3. Implement free funcions leaf/levelGridView to get the grid view.</a></li>
</ul>
</li>
<li><a href="#sec-7-2">7.2. dune-common</a>
<ul>
<li><a href="#sec-7-2-1">7.2.1. Implement user defined literals _0, _1, &#x2026; using C++14</a></li>
<li><a href="#sec-7-2-2">7.2.2. Remove VirtualFunction in favor of std::function</a></li>
</ul>
</li>
<li><a href="#sec-7-3">7.3. dune-geometry</a>
<ul>
<li><a href="#sec-7-3-1">7.3.1. Remove genericgeometry subdirectory</a></li>
<li><a href="#sec-7-3-2">7.3.2. local() method on Geometry (20 minutes)</a></li>
<li><a href="#sec-7-3-3">7.3.3. Subdivided Geometries</a></li>
</ul>
</li>
<li><a href="#sec-7-4">7.4. dune-localfunctions</a>
<ul>
<li><a href="#sec-7-4-1">7.4.1. Switch evaluate() method for partial local finite elements to multiindex</a></li>
<li><a href="#sec-7-4-2">7.4.2. Make interpolate accepts operator() notation for its arguments,</a></li>
<li><a href="#sec-7-4-3">7.4.3. Define how interpolate() can access derivaties of it's arguments</a></li>
<li><a href="#sec-7-4-4">7.4.4. dune-uggrid (15 minutes)</a></li>
</ul>
</li>
<li><a href="#sec-7-5">7.5. dune-grid</a>
<ul>
<li><a href="#sec-7-5-1">7.5.1. Intersections (until 10 a.m.)</a></li>
<li><a href="#sec-7-5-2">7.5.2. Check to detect default construction of entities/intersections</a></li>
<li><a href="#sec-7-5-3">7.5.3. Add an operator bool() to test for entity and intersection validity.</a></li>
</ul>
</li>
<li><a href="#sec-7-6">7.6. dune-istl</a>
<ul>
<li><a href="#sec-7-6-1">7.6.1. Reduce number of base classes used by BlockVector</a></li>
<li><a href="#sec-7-6-2">7.6.2. Splitup Setup Code from BCRSMatrix to a e.g. a factory class.</a></li>
<li><a href="#sec-7-6-3">7.6.3. range-based for cannot iterate over the entries of a row of a BCRSMatrix</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="#sec-8">8. Infrastructure</a>
<ul>
<li><a href="#sec-8-1">8.1. Server in Heidelberg is moving.</a>
<ul>
<li><a href="#sec-8-1-1">8.1.1. for the website instead of wml.</a></li>
<li><a href="#sec-8-1-2">8.1.2. Polling system</a></li>
<li><a href="#sec-8-1-3">8.1.3. Gitlab</a></li>
<li><a href="#sec-8-1-4">8.1.4. Flyspray</a></li>
</ul>
</li>
<li><a href="#sec-8-2">8.2. Development model (45 minutes)</a></li>
</ul>
</li>
</ul>
</div>
</div>


# Preliminaries<a id="sec-1" name="sec-1"></a>



## Core developer status for Dominic.<a id="sec-1-1" name="sec-1-1"></a>

9 persons vote yes. No one against.
Ergo we promote Dominic officially to core developer
status. Congratulations, Dominic!

## Discussion time will be appointed for each topic beforehand.<a id="sec-1-2" name="sec-1-2"></a>

## Every core developer will be<a id="sec-1-3" name="sec-1-3"></a>

# Review of 2013 meeting<a id="sec-2" name="sec-2"></a>

## Licensing<a id="sec-2-1" name="sec-2-1"></a>

There is an ongoing discussion about the licensing (due to the GPL2
and LGPL3+), there is consent that LGPL3+ is an option but Christian
needs to ask all previous contributors. He promises to do this in due
time.

## CMake vs. Autotools<a id="sec-2-2" name="sec-2-2"></a>

Thanks to Christoph the official installation notes do now discuss
CMake. Considered more or less done.

## Supported Compilers<a id="sec-2-3" name="sec-2-3"></a>

Will come later in the meeting.

## Release is out.<a id="sec-2-4" name="sec-2-4"></a>

Hooray!!! Thanks Carsten.
dune-alugrid master will be made compatible to 2.4. There will be a
dune-pdelab, dune-fem (?) 2.4 release in due time.

## QuadratureRuleKey in dune-geometry<a id="sec-2-5" name="sec-2-5"></a>

To be revisited.

## EntityIterators are true STL forward\_iterators.<a id="sec-2-6" name="sec-2-6"></a>

They can return a proxy object.

## Entity pointer removal.<a id="sec-2-7" name="sec-2-7"></a>

Done, but needs to be kicked out of master.  Robert will doe this.

## Intersections are now copyable.<a id="sec-2-8" name="sec-2-8"></a>

## Gridcheck headers renamed.<a id="sec-2-9" name="sec-2-9"></a>

Kill \*.cc files. -> Christoph Grüninger

## Need to remove boost checks from dune-common/dune-istl<a id="sec-2-10" name="sec-2-10"></a>

## Getting rid of Dune::istl\_assign\_to\_fmatrix<a id="sec-2-11" name="sec-2-11"></a>

Still there. Markus will do it.

## Change the template argument list of the geometry<a id="sec-2-12" name="sec-2-12"></a>

Due for 3.0

## Getting rid of the PartitionType template parameter on the GridView<a id="sec-2-13" name="sec-2-13"></a>

Deprecated for 2.4. Needs to be removed. Martin does it.

## Duplicated methods on Grid and GridView<a id="sec-2-14" name="sec-2-14"></a>

Remove now without deprecation. Steffen.

## geomTypes() on the IndexSet<a id="sec-2-15" name="sec-2-15"></a>

Martin removes it now.

## Range based for<a id="sec-2-16" name="sec-2-16"></a>

Improve static\_assert error message. Steffen for 2.4.1

## parallel features of the grid interface<a id="sec-2-17" name="sec-2-17"></a>

Still needs to done.

## Capabilities.<a id="sec-2-18" name="sec-2-18"></a>

Needs to be done. martin.

## intersectionIds<a id="sec-2-19" name="sec-2-19"></a>

Written proposal needed. Later.

## hasBoundaryIntersections()<a id="sec-2-20" name="sec-2-20"></a>

Makes sense. Move to GridView and rename to
mightHaveBoundaryIntersections().
Postponed to later.

# General<a id="sec-3" name="sec-3"></a>

## DONE Briefly visit 2014 protocol<a id="sec-3-1" name="sec-3-1"></a>

## Compiler versions<a id="sec-3-2" name="sec-3-2"></a>

Next major release in 1.5 years with major changes.
Parallel support for 2.4.\* releases and 3.0.
Maybe preview snapshot releases with new major features (e.g. module
namespaces, more value semantics, &#x2026;) to ease porting code. previews
will be tagged with preview-0,&#x2026;, preview-N
Release (freeze) in 1.5 years: End of March 2017!

Useful andidates are 4.7 (not C++11 compatible), 4.9 (C++11
compatible), 5.0 (C++14 compatible).
4.8 is missing the following features:
-   Generic lambdas (Needed e.g. in the datahandle to use one lambda
    with different types/codims).
-   const constexpre (incompatible changes)

Nobody insists on gcc-4.8 compatibility.
gcc-4.9 will be the target compiler for the 3.0 release. C++11 is
completely allowed, experimental C++14 features in gcc-4.9, too!
Clang 3.5 is definitely C++14 compliant. ICC >= 15.1 should be
supported.

Christoph will change the build system for C++14 compatibility.

In the master branch gcc-4.9 compatibility can be enforced from today!

## Rework website (CMake compatible replacement for WML)<a id="sec-3-3" name="sec-3-3"></a>

## Infrastructure (Should we switch to gitlab? How to organize votes?) -> see below<a id="sec-3-4" name="sec-3-4"></a>

## How to implement finite elements with higher order regularity (C1,&#x2026;)?<a id="sec-3-5" name="sec-3-5"></a>

## Release paper<a id="sec-3-6" name="sec-3-6"></a>

For 2.4 we will a write a  DUNE release paper with inviting
substantial contributors to DUNE as authors. Robert will prepare a
repository and a list of possible authors.

# Build system<a id="sec-4" name="sec-4"></a>

## CMake version in the 3.0 release will be 3.0.\*<a id="sec-4-1" name="sec-4-1"></a>

## Abolition of the magic that builds test during "make test"<a id="sec-4-2" name="sec-4-2"></a>

test will not build during make test.
Introduce meta targets build\_tests, tests ( build\_tests && test). We
introduce a variable BLA\_BLUBB\_TEST to activate building tests during
make all.
Concordantly agreed.

## Parallel tests<a id="sec-4-3" name="sec-4-3"></a>

Add possibility to dune\_add\_test to run parallel tests. Introduce an
upper bound for the processor number (default: 2) for parallel
tests. Running tests in parallel should not have side effects.  Whether
we also need an upper allowed runtime after which we kill the test
remains open.
Concordantly agreed.

## Documentation of the CMake build system will be written by Dominic!<a id="sec-4-4" name="sec-4-4"></a>

Will be done by Dominic in sphinx (see [example](http://conan2.iwr.uni-heidelberg.de/cmake-documentation/html/index.html)).
Concordantly agreed.

## Use named arguments in CMake.<a id="sec-4-5" name="sec-4-5"></a>

instead of position dependant ones for all user callable functions.
Due in  3.0. Will be done by Dominic.
Concordantly agreed.

## Remove enable trick<a id="sec-4-6" name="sec-4-6"></a>

Christoph.
Concordantly agreed.

## Alberta worldim will set at configure time.<a id="sec-4-7" name="sec-4-7"></a>

Christoph Grüninger and Martin.

# Request for new features<a id="sec-5" name="sec-5"></a>

## Introduce namespaces for 3.0<a id="sec-5-1" name="sec-5-1"></a>

Three developers sincerely want to have this.
We need to hide implementation details from doxygen and the DUNE
namepace (consent).
Proposals:
-   all modules get a sub namespace.
-   all modules except common get a sub namespace.
-   istl, and localfunctions get sub namespaces.
-   the core modules are the only ones allowed to be in DUNE.

There is not enough consent to introduce namespaces in any kind to the
core modules to warrant this amount work.
duneproject will create a sub namespace for each new module
automatically.

## Remove imported std classes from DUNE namespace.<a id="sec-5-2" name="sec-5-2"></a>

move make\_array to Dune::STD

# Clarification of interface<a id="sec-6" name="sec-6"></a>

## Do pre- and postAdapt always have to be called?<a id="sec-6-1" name="sec-6-1"></a>

Yes, even if not data needs to be moved.

Later we might want to have a nicer interface. See [FS#266](https://gitlab.dune-project.org/flyspray/FS/issues/266).
Move to faq

## GridFactory: Can one insert elements before all vertices are inserted?<a id="sec-6-2" name="sec-6-2"></a>

All vertices have to be inserted before any elements.

Move to faq!

## viewThreadSafe: What guarantees does it provide?<a id="sec-6-3" name="sec-6-3"></a>

For Joe threadsafe means that if a method takes a const reference,
then this method will (not really) change the object, i.e. despite
e.g. locking. If a user concurrently passes it as a mutable reference
to another method then this is a usage error and he is on his own.

Problems/Questions:
-   A View is threadsafe if all of its objects are threadsafe. This is
    the natural C++11 approach.
-   Each iterator may hold a reference to a grid and cannot be used concurrently
    to modifying the grid (not even dereferencing it). Maybe we need
    markThreadSafe.
-   OpenMPi has a bug. Therefore we need to initialize threads in MPI
    via MPI\_Init\_thread with MPI\_THREAD\_SERIALIZED (supported by all
    tested MPI versions). We will demand this from the MPI implementation.
-   Each grid has a reference to a communicator (communication
    layer). Each communication on it is a modification of it and has to
    be executed sequentially. The user has to ensure this. Nevertheless
    one can iterate over a grid and use it read only during a
    communicate call.
-   Any modification modifies the communication layer instance
    referenced by the grid. The following mthods may communicate
    -   Grid::communicate(), and GridView::communicate()
    -   Grid::preAdapt(), Grid::adapt(), Grid::postAdapt()
    -   Grid::loadBalance()
-   The mark method may not communicate.

See attachment [FS#1717](https://gitlab.dune-project.org/flyspray/FS/issues/1717)
Joe will implement the flag and thread safety with YaspGrid.

# Change requests for interfaces<a id="sec-7" name="sec-7"></a>

## Functional subroutines<a id="sec-7-1" name="sec-7-1"></a>

### quadrature rules<a id="sec-7-1-1" name="sec-7-1-1"></a>

Add a function that takes a requirements object and an entity to get the
according quadrature rule. There is a consensus that this might be a
good idea. Christian and Martin will work on a proposal for discussion
partly based on Carsten's code

### Reference Elements.<a id="sec-7-1-2" name="sec-7-1-2"></a>

Currently one gets the reference element from a container. The
question is raised whether it is possible to get it via ADL.
Possible overloads need to be provided in the namespace of the
grid. We will use decltype to detect wether such an overload exists.

The interface will be defined in geometry and the first overload in
grid.

The reference will be returned by value. (No: 0 Abstentions: 2)
Martin will do this change.

### Implement free funcions leaf/levelGridView to get the grid view.<a id="sec-7-1-3" name="sec-7-1-3"></a>

For now it will just forward to the member functions.
Agreed.

## dune-common<a id="sec-7-2" name="sec-7-2"></a>

### Implement user defined literals \_0, \_1, &#x2026; using C++14<a id="sec-7-2-1" name="sec-7-2-1"></a>

OS: Should be used instead of the currently used
std::integral\_constant<std::size, \*> in  typetree. See also
boost::hana. If that is not feasable continue to use them but move
them to dune-common. These can then be reused with
MultiTypeBlockVector.
Should be put into namepace Dune::Indices and import them from there.
Steffen will do the work.

### Remove VirtualFunction in favor of std::function<a id="sec-7-2-2" name="sec-7-2-2"></a>

Carsten does it.

## dune-geometry<a id="sec-7-3" name="sec-7-3"></a>

### Remove genericgeometry subdirectory<a id="sec-7-3-1" name="sec-7-3-1"></a>

Without any regard to backwards compatibility Martin will do this.

### local() method on Geometry (20 minutes)<a id="sec-7-3-2" name="sec-7-3-2"></a>

See presentation of Aleksejs:
-   if mydim < cdim, always return true
-   if mydim = cdom run Newton
    -   if found values is inside return it and true
    -   if method result lies outside return false
    -   if convergence is too slow return false

Actually Alecsejs needs to be able to call the method with values that
are outside and needs know somehow that they are outside. (He did not
tell so at first, though.)

Oliver's understanding is that it is a misuse/undefined if the requested
coordinate is outside of the element. An algorithm should be chosen
such that the computed value is always inside. What we want to have is
a value the minimises the distance on the plane.
Peter's proposal is to return a bogus outside value if the
computation fails and signal the failure.
Andreas wants extrapolation for outside values if feasilbe and a
boolean telling whether the value is correct.

Our proposal:
The method should be a best effort that
-   return the solution if it is unique.
-   return one solution if the solution is not unique.
-   Always minmise the distance to the real solution.
-   Do something id there is a failure (e.g. convergence issue) that
    might be NAN, or a bool. But will be discussed after an improved
    propsal from Aleksejs.

### Subdivided Geometries<a id="sec-7-3-3" name="sec-7-3-3"></a>

For geometries with geometry type None.

Christian will work on this.

## dune-localfunctions<a id="sec-7-4" name="sec-7-4"></a>

### Switch evaluate() method for partial local finite elements to multiindex<a id="sec-7-4-1" name="sec-7-4-1"></a>

Currently its argument is a vector with coordinate direction per
partial derivative. This should be switched to a multiindex with
number derivatives per coordinate direction in accordance with global
finite elements. (This more like describe in your favorite math
textbook.)
Agreed concordantly.

### Make interpolate accepts operator() notation for its arguments,<a id="sec-7-4-2" name="sec-7-4-2"></a>

Agreed.

### Define how interpolate() can access derivaties of it's arguments<a id="sec-7-4-3" name="sec-7-4-3"></a>

Needed for Ck-space with k>0.
Done.

### dune-uggrid (15 minutes)<a id="sec-7-4-4" name="sec-7-4-4"></a>

There is funding to improve UG and move it nearer into dune-grid, i.e.
-   Extract the actual grid from ug and
    -   move it to dune-grid into a subdirectory,
    -   make a module dune-uggrid. According to Oliver this module would be
        needed to made a mandatory core module to ease DUNE's test on
        unstructured simplicial grids.

Currently no agreement can be reached what to do with uggrid within DUNE.

## dune-grid<a id="sec-7-5" name="sec-7-5"></a>

### Intersections (until 10 a.m.)<a id="sec-7-5-1" name="sec-7-5-1"></a>

1.  [FS#1714](https://gitlab.dune-project.org/flyspray/FS/issues/1714) Generalize the dune-grid intersection concept for network grids.

    Draft proposal is in the picture of the flip chart. It is a consensus,
    But we need to polish it and compare with all use cases in the current
    grid. Use cases will be provided by at least Christian and Robert.

2.  [FS#1591](https://gitlab.dune-project.org/flyspray/FS/issues/1591) - Unique Intersections.

    Covered by the proposal above.

3.  [FS#1369](https://gitlab.dune-project.org/flyspray/FS/issues/1369) - Add id( intersection )

4.  Additional methods that we (might) want

    1.  Communicate on intersections
    2.  IdSet adapted for intersection.
    3.  It is consensus to add a method intersectionInOutside() to get
        the same intersection in the other element. It can be used to get
        e.g. the normal or the geometry. Ergo this might make the
        &#x2026;InOutside() superfluent and these could be removed (not decided
        yet). Of course this could have negativer side effects. (E.g. for a
        cell-centered finite volume method one might only want to get the
        entity of the other element.)

    Everything that could be done with a subentity of codim 1 should also
    be possible with a intersection.

5.  Reduce redundancy in exported integers of intersection interface

    OS: Currently, the Intersection interface class exports four integer
    numbers:
    -   codimension: The codimension of the intersection in the grid (always 1)
    -   dimension: The grid(!) dimension. (Also present in the entity.)
    -   mydimension: the dimension of the intersection itself
    -   dimensionworld: The dimension of the world space of the grid
        This list is a bit redundant. Also, the fact the
        Intersection::dimension is not the dimension of the intersection has
        confused quite a number of people. Can we do some cleanup?

    Vote:
    Remove all: 3
    Remove dimenion, codimensions: 7 (Abstentions 5, against 0)
    Ergo the propsal is to remove codimension, and dimension.

### Check to detect default construction of entities/intersections<a id="sec-7-5-2" name="sec-7-5-2"></a>

SM: Add an operator bool() for entities and intersections to check whether
the object was default-constructed.

### Add an operator bool() to test for entity and intersection validity.<a id="sec-7-5-3" name="sec-7-5-3"></a>

Add an operator bool() for entities and intersections to check whether
the object was default-constructed (returns false) or contains data of
an actual entity / intersection (returns true).

Has been declined.

## dune-istl<a id="sec-7-6" name="sec-7-6"></a>

### Reduce number of base classes used by BlockVector<a id="sec-7-6-1" name="sec-7-6-1"></a>

### Splitup Setup Code from BCRSMatrix to a e.g. a factory class.<a id="sec-7-6-2" name="sec-7-6-2"></a>

### range-based for cannot iterate over the entries of a row of a BCRSMatrix<a id="sec-7-6-3" name="sec-7-6-3"></a>

# Infrastructure<a id="sec-8" name="sec-8"></a>

## Server in Heidelberg is moving.<a id="sec-8-1" name="sec-8-1"></a>

### <http://gohugo.io> for the website instead of wml.<a id="sec-8-1-1" name="sec-8-1-1"></a>

website will be maintained as markup in a repository. Building the
website will be triggered via a push.
For parts of the website contributors will be able to use merge
requests.

Heidelberg University will setup the system, evaluate, and get back to
us.

### Polling system<a id="sec-8-1-2" name="sec-8-1-2"></a>

Christian wil find a suitable coordinated with Heidelberg.

### Gitlab<a id="sec-8-1-3" name="sec-8-1-3"></a>

Do we want to add gitlab to our workflow to allow merge requests and
subsequent pushes by core developers? Directly pushing might still be
possible. The SVNManager will die with the introduction of gitlab.
Yes: 10 No: 0 Abstention: 2

### Flyspray<a id="sec-8-1-4" name="sec-8-1-4"></a>

Can we get rid off flyspray and use the bug tracker of gitlab?
Consentently agreed.

## Development model (45 minutes)<a id="sec-8-2" name="sec-8-2"></a>

We will try to use dedicated feature branches even more then already
now. This comes natural with the preferred gitlab pull request based
development model.
