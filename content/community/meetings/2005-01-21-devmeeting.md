+++
title = "Protokoll des Dune Treffens vom 21.1.05 in Heidelberg"
[menu.main]
parent = "meetings"
+++

1) Berichte der AGs:
--------------------

*Heidelberg:*

  - Paraview Visualisierung basierend auf vtk
  - Datenausgabe fuer Data Explorer (Roland Schulz)
  - DG-Verfahren mit abgeschnittenen Basisfunktionen laeuft (Christian Engwer)
  - AMG wird zur Zeit in Dune eingebaut (Markus Blatt)
  - Vortrag auf GAMM-Tagung zu Dune (Peter Bastian)

*Berlin:*

  - 3D Kontaktproblem (UG) und Richardsgleichung (SGrid)laufen in Dune (Oliver Sander)
  - mehrere Gitter gleichzeitig --> Paper in Vorbereitung
  - UG Schnittstelle ist seriell fertig, aber noch nicht parallel

*Freiburg:*

  - ALU3D Gitter von Bernhard Schupp ist an Dune angebunden (Robert Kloefkorn) Das Gitter wird unter LGPL veroeffentlicht
  - Anforderung: das AMG von Markus Blatt steht auf der Wunschliste
  - Vortrag auf SIAM-Tagung zu Dune (Robert Kloefkorn)

2) Diskussion der "const" Umstellung:
-------------------------------------

*Ergebnis:*

  - Alles wird "const" gemacht
  - die "mark" Routine geht von entity nach grid

3) Änderungen:
---------------
  - "element" wird in "geometry" umbenannt
  - codim statt traits
  - hasChildren wird zu Is Leaf
  - Iteratroren werden auf Facates umgestellt
  - Integration Element nicht auf codim = dim -> weg mit Spezialisierung

4) Index-Konzept:
-----------------

*Ergebnis:* Vorschlag von Robert wird angenommen:

 - Es wird eine Klasse "IndexSet" geben
 - Folgende IndexSets soll es auf Grid geben:
    - LevelId  (Konsekutiv, nicht persistent)
    - GlobalId (nicht konsekutiv, persistent)
    - LeafId	  (nicht konsekutiv, persistent)

5) Parallele Schnittstelle:
---------------------------

*Ergebnis:*
 - Festlegung auf MPI in Dune
 - communicate Methode muss "entity" statt "index" liefern
 - Diskussion zu beginCommunicate, communciate , ..., endComunicate, um den Austausch mehrere Daten mit einem Schritt zu ermoeglichen
 - Einfuehrung einer Methode loadBalance (Vorschlag von Robert)

6) Referenzen vs. Konstruktoren:
-------------------------------

*Ergebnis:*

 - Es soll ein Iterator eingefuehrt werden, der kein ++ hat (EntityPointer)

7) Festlegung der Referenzelemente:
----------------------------------

*Ergebnis:*

 - Referenzelemente wurden festgelegt und sollen von Oliver implementiert werden. Dies soll in grid.hh geschehen

8) Leafiterator:
----------------

*Ergebnis:*

 - Leafiterator mit codim=0 soll in Interface Klasse
 - Leafiterator mit codim=0 soll in Default Klasse
