Protocol Dune Developer Meeting
===============================

Place: RWTH Aachen

Day 1, 2013-09-26
-----------------

Attendants: Stefan Girke, Oliver Sander, Christian Engwer, Jorrit
Fahlke, Stefan Lang, Markus Blatt, Martin Nolte, Steffen Müthing, Peter
Bastian, Andreas Dedner (until 17:15), Tobias Malkmus, Christoph
Gersbacher, Dominic Kempf, Christoph Grüninger, Didrik Jonassen (until
16:30), Carsten Gräser, Robert Kloefkorn (via Skype)\
Transcript writer: Christoph Grüninger

### Limit number of core developer?

The following points were suggested:

-   the decision processes of Dune core development are not optimal
-   May the large number of core developers be part of the problem?
-   use/improve vote system

The following point was decided upon:

-   do not add new core developers until discussed

### New core developers

There were two nominations for new core developers:

-   Steffen Müthing (Universität Heidelberg)
-   Christoph Gersbacher (Universität Freiburg)

Both nominations were approved, and Steffen and Christoph are now core\
developers

-   Steffen Müthing (yes: 8, abstaintion: 1, no: 0, not here: 1)
-   Christoph Gersbacher (yes: 6, abstention: 3, no: 0, not here: 1)

### New release 2.3

Markus Blatt volunteered to be the manager for the next release.\
Christoph Grüninger volunteered to help him.

The new release will be 2.3, and is planned to appear before Christmas
2013

It is likely that afterwards there will be a 3.0 release, which may
introduce\
a few non-backward-compatible changes.

### Structured grids in dune-grid

OS stated the opinion that the structured grids in dune-grid are in a
unsatisfactory state:\
Both SGrid and YaspGrid have interesting features that the other one
doesn't have. Instead\
of one really good structured grid there are two mediocre ones. There
was no general consensus\
on this matter.

In any case, there was agreement that SGrid cannot be dropped until
another dune-grid grid\
fully implements the interface and all features of SGrid.

One suggested solutions was to move SPGrid into dune-grid. This issue
has not been decided\
upon; the SPGrid developers did not appear to like the idea.

PB mentioned that he will hire a hiwi in the near future, who may be
told to implement some\
more of the missing features in YaspGrid (in particular, entities of
codimensions other than\
0 and the grid dimension).

### dune-grid-dev-howto

The dune-grid-dev-howto only contains the IdentityGrid implementation,
which exists for\
teaching purposes only. The module gets little attention, and therefore
the IdentityGrid\
code is subject to frequent bitrot. It was originally planned that
dune-grid-dev-howto\
should also contain a text on how to write a dune grid. However, that
text has not been\
written yet.

A book on Dune is in the works, written by a number of the Dune
developers. It is planned\
that this book will contain a chapter on how to write a grid. If this
works out there is\
no reason why the text should also appear in dune-grid-dev-howto.

In light of this situation, OS proposed to move IdentityGrid into
dune-grid itself, and get\
rid of dune-grid-dev-howto altogether. This was voted upon, with the
following result:\
yes: 6, abstention: 2, no: 3.\
Hence the proposed changes will be put into effect, but only after the
2.3 release.\
OS volunteered to maintain IdentityGrid in dune-grid.

-   Adding teaching grids in dune-grid-dev-howto, they do not need to
    match expectations on quality

### dune-functions

It was briefly explained that there has been an effort to design a new
module dune-functions.\
This module should combine the knowledge from dune-pdelab, dune-fem, and
dune-fufem, and define\
interfaces for functions on grids, and discrete function spaces. A first
meeting has taken place\
in Münster, attendants being:

-   Christian Engwer, Steffen Müthing (for dune-pdelab)
-   Christoph Gersbacher, Stefan Girke (for dune-fem)
-   Carsten Gräser, Oliver Sander (for dune-fufem)

There has been good progress, but the results are not suitable for a
wider audience yet.

### misc.hh

It was decided that the file misc.hh in dune-common should be removed in
an effort\
to clean up the code (see
[FS\#1338](https://dune-project.org/flyspray/index.php?do=details&task_id=1338)
)

It was decided to

-   move the math stuff in there to the file math.hh
-   deprecate/remove the method genfilename, because it is only needed
    by dune-fem
-   move string handling to a new file string.hh

OS volunteered to take care of this.

### Remove BasicGeometry

It was decided to remove the BasicGeometry class from the dune-geometry
module.\
It is superseded by MultiLinearGeometry, which offers mostly the same
functionality,\
but which has much simpler code.

The class is deprecated already. MN volunteered to remove it after the
2.3 release.

It is the aim to remove the entire genericgeometry directory along with
BasicGeometry,\
but it is not entirely clear how much of the stuff in there is needed
elsewhere.\
This will be evaluated step by step.

### Support request in FlySpray

It was decided that the category 'Support request' will disappear from
the FlySpray\
bugtracker, because the bugtracker should remain reserved for actual
bugs only.

Vote: yes: 8, abstention: 3, no: 0

CE volunteered to take care of that.

### Drop support for older compilers

It was decided after the 2.3 release we will cease to support gcc
versions\
older than 4.4.

Further, we will then state that the dune core modules will be

-   compatible with Clang 3.2 or newer but this is not actively tested
-   compatible with ICC 13 or newer, but this not actively tested

Christoph Grüninger will update the documentation accordingly

### Use more C++11 features

We change our policy regarding C++11 features after the 2.3 release.
Some feature are still not allowed, due to our compiler support (see
above).

-   Drop compatibility code after dune 2.3: shared\_ptr, array, tuple,
    some typetraits, static\_assert
-   Pull std:: stuff to Dune namespace (UPDATE: namespace Dune::std::
    \[see below\])
-   Allowed C++11 featured? (will be tested by Steffen Müthing)\
    &gt; \* Rvalue references: work with noexcept, thus we introduce the
    macro DUNE\_NOEXCEPT (Steffen Müthing will write Macro). Note: the
    feature maybe a problem with ICC 13.\
    &gt; \* variadic templates\
    &gt; \* static assert: drop fall back code, keep rest as it is\
    &gt; \* decltype\
    &gt; \* &gt;&gt;: we have no strong opinion and allow developers to
    write ClassNameA<ClassNameB<Param>&gt;\
    &gt; \* default template arguments for function templates:\
    &gt; \* strongly typed enums: postponed due to partial support in
    ICC 13\
    &gt; \* deleted functions: with DUNE\_DELETE\
    &gt; \* inline namespaces:
-   Not allowed C++11 featured\
    &gt; \* defaulted functions:\
    &gt; \* auto: no until we drop GCC 4.4 because there are bugs\
    &gt; \* initializer lists: add support to our classes, allow use
    inside dune core modules (supported by ICC 13?)
-   Write software in a way to be compatible with range based loops and
    lambda expressions

### Alberta 2 support

-   Alberta 3.0 is almost finished
-   Testing Alberta 2 is a burden, Markus Blatt will include that in his
    testing system
-   Keep Alberta 2

### SubEntity of all co-dimensions

-   All grids must provide SubEntities of all co-dimensions
-   These SubEntities do not necessarily provide Geometries, capability:
    hasGeometry
-   name of capability hasEntityIterator

### Geometry of SubEntity

-   Geometry is fixed (globally unique) for all entities
-   for each SubEntity we provide (by some means) the geometric and
    topological mapping into the reference element of the entity
-   Carsten Gräser will provide test of consistency of subentity
    orientation
    [FS\#980](https://dune-project.org/flyspray/index.php?do=details&task_id=980)

### default constructor for EntitySeed

-   see
    [FS\#1221](https://dune-project.org/flyspray/index.php?do=details&task_id=1221)
-   add default constructor to EntitySeed
-   add function isValid() to EntitySeed
-   add explicit (with macro DUNE\_EXPLICIT) bool cast to facade class
-   after dune 2.3 release
-   vote (yes: 11, no:0)

### Type of jacobian(Inverse)Transposed

-   provides all const methods which are not element access
-   const dense matrix interface, no element access, no iterators
-   method returns a copy not a reference
-   can be casted to FieldMatrix
-   Christoph Gersbacher provides a test

Day 2, 2013-09-27
-----------------

Attendants: Stefan Girke, Oliver Sander, Christian Engwer, Jorrith
Falke, Markus Blatt, Martin Nolte, Steffen Müthing, Peter Bastian,
Tobias Malkmus, Christoph Gersbacher, Dominic Kempf, Christoph
Grüninger, Carsten Gräser, Robert Kloefkorn (from 14:30 on, via Skype)\
Transcript writer: Christoph Grüninger

### Replace rows() and cols() DenseMatrix and FieldMatrix

-   wanted? unclear name
-   Capability isStatic? Until constexpr is usable
-   subgroup (under the lead of Christian Engwer) will work out / decide

### Introduce macro DUNE\_CONSTEXPR

-   introduce macro DUNE\_CONSTEXPR (Martin Nolte)
-   do not rely on it in core modules, for user code

### Constructor for FieldVector

-   data initialized with default value (yes: 9, abstention: 0, no: 0)

### Git workflow

-   get rid of old release branches, only one branch per x.y release,
    keep tags
-   stick to concept of private branches, please remove private branches
    if no longer used
-   use tags in squared brackets, add summary in first line of commit
    message
-   git push only secured over https

### convenience method size() for the LocalFiniteElement

Currently, if you have an object foo of type LocalFiniteElement, you had
to\
either call foo.localBasis().size() or foo.localCoefficients().size() to
obtain\
the number of shape functions of the finite element. OS proposed to add
a\
convenience method to the LocalFiniteElement class itself, which would
allow\
to simply write foo.size();

This proposal was approved. The work will be done by OS.

### Naming convention

While there are naming rules for methods, classes, etc., these are
inconsistently applied.\
The Dune developers declared their intention to straighten out some of
the oddities.

-   The Dune naming scheme is CamelCase
-   Capitalization should never change inside an acronyms, e.g.
    getBCRSMatrix(), but bcrsMatrix
-   Exported types are not supposed to end in 'Type', e.g. Size instead
    of SizeType, Index instead of IndexType.\
    The cases where exported types do end in 'Type' are considered bugs,
    and will be changed.
-   An exception is 'ctype'. It does not follow the naming rules, but
    nobody came up\
    with a good alternative name.

An exception will be made to classes that are very closely in spirit to
classes found\
in the STL (like, e.g., container classes).

-   It was decided that such classes should follow the pure STL naming
    style exclusively.
-   Examples are ReservedVector, BitSetVector, lru, etc.
-   Those not already using STL naming will be converted.
-   All classes following the STL style will be in a separate
    namespace Dune::std.
-   This was chosen from among three proposals: Dune::STD, Dune::Std,
    Dune::std
-   (vote (dislikes): STD: 5 (--4), Std: 5 (--3), std: 5 (--2))
-   vote: yes: 9, no: 0

Finally, there has been constant unhappiness for some people that while
grids export a type\
LeafGridView, the corresponding method is called leafView() (without the
'Grid'). It was\
decided to rename the method leafView to leafGridView, and levelView to
levelGridView.\
Deprecation of the old methods shall happen before the 2.3 release.

OS will supervise the effort.

### Entity should no longer export ctype and dimworld

The Entity facade classes in the dune grid export types 'ctype' and the
dimension\
of the world space in 'dimworld'. This is inconsistent design, because
Entities\
are supposed to only carry topological information, and ctype and
dimworld are\
both geometric in nature. OS proposed to remove these two exported
types.

-   vote: yes: 8, abstention:1, no: 1

OS volunteers to remove the types.

### CMake

-   Please test, works in general
-   Vague plan to make CMake the successor of autotools (Dune 3.0?)

### GridTraits should get implementations instead of Traits

-   Oliver Sander will provide prototype

### Singleton

-   Steffen Müthing will provide a macro DUNE\_EXPORT

### deprecate and remove lbegin/lend from Grid

-   deprecate and remove lbegin/lend from Grid, use from GridView
    instead
-   vote: yes: 9, no: 0

### viewThreadSafe more precise definition

-   one thread can communicate while others can traverse the grid
-   IndexSet has to be obtained once before using in others threads, too
-   same for IdSets, GridView
-   Christian update(s/d) the documentation accordingly

### Tuple library

-   simplify using variadic template stuff
-   inclusion intended after Dune 2.3 release
-   people should have a look on the branch
    [dune-common:p/gersbach/FS1336-tuples-variadic-templates](http://cgit.dune-project.org/repositories/dune-common/log/?h=p/gersbach/FS1336-tuples-variadic-templates)

### boundaryId

-   move boundaryId to enable\_grid\_extension (Christoph Grüninger)
-   boundary problem is still an open problem, discussion should be
    picked up again

### range based for loops over GridView

-   EntityRange does not provides contains()?
-   provides begin() and end()
-   member type iterator
-   Dune::entityRange() or GridView.entityRange()?
-   analog for Intersections?
-   follow up discussion needed (Flyspray, Martin Nolte)
