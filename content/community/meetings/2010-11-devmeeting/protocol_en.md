Protokol of the Dune Developer Meeting 2010
===========================================

**Comment:** Stuff marked as comment was added after the meeting and is
thus no official representation of the dune developer's opinion

Dune User Meeting 2010 results
------------------------------

See the discussion results on the [Dune User Meeting 2010](../2010-10-usermeeting) site.

-   **reduce dependencies on external tools**\
    There should be fewer dependencies on tools like *WML*
    -   all *WML* related stuff should be moved to *dune-web*
-   **packages**\
    the ultimate goal is to have dune packages available for all major
    distributions
    -   for the time being, providing packages should be done by the
        community, not the developers
    -   if someone has the time, he could take a look at automated
        package creation and deployment tools (like
        [launchpad](https://launchpad.net/))
    -   if someone has the money, he should hire a student to create
        dune packages
-   **buildsystem**\
    The buildsystem is good as it is at the moment
    -   there shall be no major changes, until someone comes up with
        really good ideas and manpower
    -   the buildsystem could be simplyfied, if one removes either the
        option to use dune with *make install* (which is needed for
        package buildilg) or the option to use dune without *make
        install*
-   **bug tracking**
    -   there should be a wiki page containing all information that
        should be provided when filing bugs, so people can be directed
        there on first requests
-   **feature list**
    -   there should be feature lists, that are developed in the wiki by
        users and developers (like the
        `Grid-Manager_Features` comparison), that are moved to the official doxygen
        documentation, once they are finished
-   **deprecated warnings**
    -   the deprecated macro from *dune-fem* should be moved to
        *dune-common* (possibility to have messages like "is
        replaced by...")
-   **mailinglist**
    -   there will be a new mailinglist that is only intended for people
        interested in developement
    -   the currrent mailinglist will become a support mailinglist
-   **incremental grid tests**
    -   there should be incremental tests in the *dune-grid-dev-howto*
-   **paralellisation**
    -   there should be more information concerning thread safety in the
        doxygen documentation
    -   there will be no efforts towards OpenMP, user input is welcome
    -   there will be no efforts towards computation on GPUs, user input
        is welcome

Votes on new developers, access rights, permissions
---------------------------------------------------

-   **Carsten Gräser** is granted developer status (*no votes against*)
-   for further information on organisational topics, see below

Performance issues
------------------

### EntitySeed

What has been discussed under the name *EntityKey* shall be implemented
and named

* EntitySeed
  * default constructor returns *EntityPointer*
  * method on the *Entity* to get an *EntitySeed*:

    ```
    EntitySeed< Codim, GridImp > Entity.seed()
    ```

  * method on the grid to get an entity:

    ```
    Grid.entityPointer< Codim >( seed )
```

  * exported types:
    * `Grid`
    * `Entity`
    * `codim`

### Copy vs. Reference

A benchmark by *Martin Nolte* shows an overhead reduction from 40 to 20
percent (see
[here](cube-geo.pdf) and
[here](cube-geo-rel.pdf))

-   from the next release on, there shall be a gradual transition
    towards returning objects instead of references
-   in the long run, there shall be refcount for the *LeafIndexSet*
-   from the next release on, there shall be a *MatrixInterface*
    -   the *MatrixInterface* shall provide all const methods of the
        *DenseMatrix*, without `operator[]`
    -   *Jacobian*, etc. shall be replaced my *MatrixInterface* (*no
        votes against*)

### Semantics of method contains()

The method shall return *undefined* if the entity in question is not an
element of the grid

Interface Cleanup
-----------------

### GenericReferenceElement

-   from the next release on, there shall be only
    *GenericReferenceElement*
-   from the after next release on, *GenericReferenceElement* shall be
    deprecated, moved to *ReferenceElement*
-   from the release after the after next release on, there shall only
    be *ReferenceElement*

### GeometryType vs. topologyId

Both shall be merged into *GeometryType*

-   method `id()` shall return the *topologyId*
-   method `basicType()` shall be extended to return *not presentable*

### methods on reference element

-   `initializeTopology()` shall be protected
-   `initialize()` shall be protected
-   `checkInside( const FieldVector< ctype, dim-codim > &local, int i )`
    shall be deprecated
-   all `global()` shall be deprecated

Interface extensions
--------------------

### Facilitate implementation/testing of new features

-   there shall be a define `EXPERIMENTAL_GRID_EXTENSIONS` to test
    experimental grid extensions, i.e. to make `getRealImpl()` public\
    **Comment:** we should perhaps call this one
    `DUNE_GRID_EXPERIMENTAL_EXTENSIONS`, to be consistent with the other
    define (*Felix Albrecht*)
-   these flags have to be documented
-   these flags have to be removed in a release

### subIndex interfac

`subIndex()`, `subEntity()` and `count()` shall be implemented for
higher codimensions

### Persistent container

shall be moved to `grid/utilities/`

### I/O

There shall be a new class `BackupRestoreFacility`

* it has to ensure functionality only if restore is applied to the same number of nodes
* backup:

    ```
    static void backup( const Grid& grid, const std::string& path, const std::string& fileprefix )

    static void backup( const Grid& grid, std::ostream& stream )
    ```

* restore:

    has to ensure, that `IndexSet` and `IdSet` are the same after restore

    ```
    static Grid* restore( const std::string& path, const std::string& fileprefix )

    static Grid* restore( const std::istream& stream )
    ```

dune-localfunctions
-------------------

-   naming will not change
-   decision on *VertexOrdering* and *Twist* is postponed
-   **Jö Falke** shall move his stuff to *dune-localfunctions*
-   new stuff shall be called *FiniteElement*
-   *GlobalValuedFiniteElement* shall be changed
-   there shall be an example documentation for *FaceOrientation*
-   geometry information shall be provided via an interface similar to
    *Dune::Geometry*
-   in the long run, there shall only be one variant of the old finite
    elements

GridFactory interface
---------------------

### Problems with boundary projection and load balancing/checkpointing

-   *Oliver Sander* shall think of a prototype for a *Domain* as a basis
    for discussion
-   parameterisation of `insertBoundarySegment()` shall be removed
-   the file reader shall have the possibility to obtain the *Domain*
-   changes should only be possible until `createGrid()`

### ParameterTree interface for the GridFactory

The constructor shall be removed (*Jö Falke*)

### Parallel grid creation

Will be undertaken, when the above is done (*Problems with boundary
projection and load balancing/checkpointing*)

Flyspray tasks
--------------

### 841: Remove dependency to WML

-   all *WML* related stuff shall be moved to *dune-web* (*Christian
    Engwer*)
-   *WML* checks shall be removed (*Christian Engwer*)

### 830: Use svn tags for bugfix releases

-   *tags*, *releases*, *branches* shall correspond to the svn standard
    in the future
-   there shall be a new branch `branch/release-2.1`
-   the *releases* subdirectory shall be moved after the next release
    (*Oliver Sander*)

### 827: rbegin of FieldVector and ISTL classes has surprising semantics

-   the methods `rbegin()`, `rend()` shall be deprecated
-   a possible name for these methods shall be `beforeBegin()`,
    `beforeEnd()` (*Markus Blatt*)

### 819: Remove enum FieldVector::size

-   `size()` should always be a method (*Carsten Gräser*)
    -   `::size` shall be deprecated
    -   there shall be a define `DUNE_COMMON_RELEASE_COMPATIBILITY` to
        enable the old behaviour

### 818: Topological embedding and indices of subentities are missing

-   resolved by `subIndex()` (*Carsten Gräser*)

### 801: Int2Type vs. integral\_constant

-   `Int2Type` shall be moved to *dune-fem* (*Robert Klöfkorn*)
-   `Int2Type` shall be deprecated in the other modules (*Jorrit
    Fahlke*)
-   change to `integral_constant` in the core modules (*Jorrit Fahlke*)

### general discussion about new features in the standard

* the oldest compiler in the list of supported compilers has to be supported
* the list of supported compilers shall contain approximately the releases of supported compilers of the last four years (including possible exceptions, ie. exotic architectures that are known to work only with an older compiler)
* the list of supported compilers shall be updated on every dune developer meeting
* the list of supported compilers shall look like the following from the next release
  * gcc 4.1 and newer
  * icc 10.0 and newer
* if all supported compilers implement a certain feature from the standard, this should be used
* if there is a supported compiler that does not implement a feature from the standard yet this feature shall be reimplemented using *CamelCase*

### 791: Unify interface class LevelIterator and LeafIterator

-   method `lbegin()` shall be deprecated
-   `{Level,Leaf,Hirarchic}Iterator` shall be moved/merged into
    `EntityIterator` (*Christian Engwer*, *Martin Nolte*?)

### 766: Separate GenericGeometries and the SmallObjectPool

-   there shall be a *char* pointer in the geometries, the *allocator*
    shall be removed (*Carsten Gräser*)

### 746 / 661: Extract 'function from expression' functionality from dgf parser

-   if there will be a good implementation, it shall be moved to
    *dune-common* (*Carsten Gräser*, *Felix Albrecht*)
-   this shall be removed from the *dgf* *parser* in the future

### 743: Add a way to determine whether a grid is hybrid

-   `hasSingleGeometryType` shall be a capability (*Robert Klöfkorn*)
-   there shall be a documentation: only implemented for codim 0!
-   if there is only one type, this shall be exported as *GeometryType*
    (topologyId of the codim 0 entity)

### 699: A general dynamic parameter interface for grids

-   see discussion about *GridFactory*

### 670: Remove obscure "DUNE developer mode"

-   the functionality shall always be provided
-   `--enable-dunedevel` shall be removed

### 532: Entity&lt;0&gt;::count should have a dynamic codim argument

-   no change

### 515: Move method 'contains' from IndexSet to GridView

-   method `contains()` shall be implemented on grid views as well
    -   PartitionType shall be a parameter (like `begin()`, `end()`)
-   shall be implemented for all codims, which the `GridView` has

### GridFamilyTraits to restrictive

-   shall be extended by a type `LocalGeometry` (dimGrid -&gt; dimGrid)
-   *Oliver Sander* shall present a proposal

Organisation
------------

### branches

-   should be used in the future
-   there should be a certain window of time after a branch is completed
    to test it, before it is merged into the trunk
-   there should be communication before a merge
-   there shall be a documentation on the homepage (on *Developing
    Dune*) (*Robert Klöfkorn*)
-   naming convention for branches: `develop-rREVISION-DESCRIPTION`,
    where `DESCRIPTION` must be nonempty and should be meaningfull
-   a branch is removed after the merge by whoever created it

### developer status

-   developer meetings shall be once a year
-   small meetings are possible whenever wished
-   discussions shall take place using the *discussion* tag in the
    FlySpray
-   there shall be the following states, which should be mentioned on
    the homepage including a list of people, according to their status
    including their active time
    -   **Developer:** those who are allowed to vote on developer
        meetings (former Core Developer)
    -   **Alumni:** those that did contribute in the past, but have
        resigned
    -   **Contributor:** the rest

### Make discretization modules core modules

-   the intention of the official homepage shall be to promote the dune
    modules of the developer group, not only the core modules
-   the discretization modules shall be presented more aggressively, the
    intention of dune (to solve pdes) shall be made clearer
-   the presented modules shall be
    -   dune-pdelab
    -   dune-fem
    -   dune-grid-glue
    -   dune-subgrid
    -   dune-spgrid (if *Martin Nolte* agrees)
    -   dune-prismgrid (if *Freiburg* agrees)
    -   dune-common
    -   dune-grid
    -   dune-istl
    -   dune-localfunctions
-   all the other modules shall be listed as external modules with their
    corresponding dependencies
