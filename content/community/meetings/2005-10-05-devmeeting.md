+++
title = "DUNE Hütten-Workshop 5.-7.11.2005"
[menu.main]
parent = "meetings"
+++

0) Sprüche

> "Wir promovieren ja nicht in UNIX."
> -- Robert


1) Schnittstelle fixieren und dokumentieren
-------------------------------------------

- Utilityklassen für alle Gitter, Spezialisierungen
  ==> Klassen werden angelegt, Spezialisierungen für die Gitter können
  jeweils angelegt werden, Dokumentation in extra Modul.

- Prozedure zur Schnittstellenerweiterung
  ==> Feature request Kategorie in Flyspray. Ist ja schon mal ein
  Anfang. Alle Leute sollen eine Meinung abgeben, auch wenn es weiss
  nicht ist.

- Prozedere zum Versionswechsel
  ist trivial, da durch CVS vorgegeben (abbranchen, superwichtige bugfixes).

- communicate & loadbalance
  Vorschlag:

```
// communicate
//============

//! class to be written by use to pack/unpack data
class CommuncateDataHandle {
public:
  //! export type of data for message buffer
  typedef XX DataType;

  //! returns true if data for this codim should be communicated
  bool contains (int dim, int codim) const;

  //! pack data from user to message buffer
  template<class MessageBuffer, class EntityType>
  void gather (MessageBuffer& buff, const EntityType& e) const;

  //! unpack data from message buffer to user
  template<class MessageBuffer, class EntityType>
  void scatter (const MessageBuffer& buff, const EntityType& e);

  //! how many objects of type DataType have to be sent
  template<class EntityType>
  size_t size (EntityType& e) const;
};

//! message buffer class exported by each grid
class MessageBuffer {
public:
  // write data to message buffer, acts like a stream !
  template<class DataType>
  void write (const DataType& data);

  // read data from message buffer, acts like a stream !
  template<class DataType>
  void read (DataType& data) const;
};


// in the grid class:
// ==================

//! levelwise communicate
template<class DataHandleVector>
void communicate (DataHandleVector& data, InterfaceType ifType, CommunicationDirection direction, int level) const;

//! leafwise communicate
template<class DataHandleVector>
void communicate (DataHandleVector& data, InterfaceType ifType, CommunicationDirection direction) const;

// ? all levels ?

// load balance
//=============

enum LoadBalanceIteratorRange {wholeGrid, leafGrid};

//! class to be written by use to pack/unpack data
class LoadBalanceDataHandle {
public:
  //! export type of data for message buffer
  typedef XX DataType;
  typedef YY BufferType;

  //! returns true if data for this codim should be communicated
  bool contains (int dim, int codim) const;

  //! return range where data is taken from
  IteratorRange range () const;

  //! pack data from user to message buffer
  template<class MessageBuffer, class EntityType>
  void pack (MessageBuffer& buff, const EntityType& e) const;

  //! unpack data from message buffer to user
  template<class MessageBuffer, class EntityType>
  void unpack (const MessageBuffer& buff, const EntityType& e);

  //! how many objects of type DataType have to be sent
  template<class EntityType>
  size_t size (EntityType& e) const;

  //! access to buffer, is like STL vector. Contains newly arrived data
  BufferType& buffer ();
};


// in the grid class:
// ==================

/** @brief load balance the grid and communicate data

    The data object has already stored the access to the data on the old mesh
    (usually in a map, storing the old index)
 */
template<class DataHandleVector>
void loadBalance (DataHandleVector& data);

/** @brief Store transmitted data in the new places

    This function is called after the grid has been load balanced. The
    grid possibly stores the packed user data in internal buffers.
 */
template<class DataHandleVector>
void postLoadBalance (DataHandleVector& data) const;
```

- periodische Ränder & BoundaryEntity
  ==> boundary.html
  geklärt und umgebaut, was nötig ist.

- Weitere Namespaces einführen ?
  ==> schön, aber nicht jetzt

- Minimalset an Funktionalität, die jedes Gitter bieten muss.
  ==> ergibt sich aus den Paper Beispielen

- AdaptionState und Parameter für mark/globalRefine
  ==> dokumentieren, mark Parameter steht zur Diskussion
  Parameter von mark() und Rückgabewert von state()
  (-> adaptationState()) sollen Klassen AdaptationState und
  AdaptationRule werden, in denen die Strategie beschrieben
  wird. Diese beiden Klassen können dann entsprechend einfach
  erweitert werden.

  AdaptationRule -> liste von Adaptionsarten

- Schnittstelle um Makrogitter portabel einzulesen
  ==> AmiraMesh wäre OK, wenn gut verfügbar. Allerdings sollte sowas
  bei DUNE dabei sein ohne runterladen zu müssen. Ist unabdingbar für
  Version 1.0!


1b) Implementierungsdetails
---------------------------

- Prozessorränder
  ==> keine extra methode
  ==> boundary.html
 erledigt mit boundary

- Lebensdauer von IdSet/IndexSet, Copyconstructor private
  ==> Objekte werden vom Gitter mitgeändert, copy cons machen wir
  private damit niemand Objekte kopieren kann.

- Copyconstructor/Assignment von Iteratoren<-> problem in ALUGrid
  ==> Scheint mit machbarem Aufwand möglich, jetzt hat ALUGrid in
  jedem Fall nicht die übliche Funktionalität eines Iterators.

- ISTL: reserve
  ==> ist implementiert. Im Gegensatz zur STL kann mit reserve auch
  Speicher freigegeben werden: Falls die verlangte Kapazität kleiner
  als die momentane aber größer als die size ist, wird Speicher
  freigegeben. Mittels eines false als Parameter kann umkopieren
  verhindert werden. Parameter gibt es auch bei resize

- ISTL: gleiche Namen beseitigen
  ==> andere Namen verwenden
  gemacht für IndexSet->ParallelIndexSet. Abstrakte Oberklasse Operator
  gelöscht, da momentan eh nur LinearOperator benutzt wird.

- return type von positionInOwnersFather -> const
  ==> const referenz zurückgeben. Aber wir sollten eine generelle Strategie
  entwickeln! Und: Generelle Benutzungsregeln erlassen?.

- Status der Methoden positionInOwnersFather etc.
  ==> Sind in Aberta und ALU implementiert (evtl nicht besonders
  effizient), Überlegen ob man das als default implementieren kann.

- IdSet::subid() in IdSet::subId() ändern !
  ==> Ja!
  ==> flyspray task schreiben, sobald eine Verletzung der
      Nameskonvention auffällt. Möglichst alle Fehler bis 1.0 entfernen.

- jacobianInverse() : Transponierte !!
  ==> jacobianInverseTransposed()

- Rekursionen bei Barton-Nackman erkennen und blockieren
  ==> ja
  versucht, aber nicht machbar in überschaubarer Zeit


1c) Buildsystem
---------------
- autogen.sh soll nur noch generieren und nicht automatisch configure
  aufrufen.

- Buildsystem von Applikationen sollte alle Abhängigkeiten von Dune
  erfahren.

- Geht es die Abhängigkeiten einer Applikation bis in Dune hinein zu
  verfolgen und dann automatisch libdune neu bauen zu lassen?


2) Beispielanwendungen auf allen Gittern
----------------------------------------

Hierarchie von Testbeispielen

* Tutorial (Heranführen von neuen Benutzern)

* Testen

* Dune-Paper

- Euler-Gleichung, explizit, first order, lokal adaptiv
  a) Einheitswürfel
  b) forward facing step

- Elliptisches Modelproblem, P1 FE, lokal adaptiv
  a) Einheitswürfel

- Oliver's Beispiele?


3) Dokumentation
----------------

- Einstiegseite, "Informationszentrum"
- Gitterschnittstelle ausführlich dokumentieren
- Installation Anleitung
  => erledigt: installation-notes.html

4) Paper
-------


5) Version 1.0
--------------


6) Lizenzfragen
---------------


7) Zukunft, wie geht es weiter
------------------------------
