+++
title = "Meetings"
[menu.main]
parent = "community"
identifier = "meetings"
weight = 2
+++

## User Meetings and Conferences

* [User Meeting 2017](2017-03-usermeeting) in Heidelberg
* [User Meeting 2015](2015-09-usermeeting) in Heidelberg
* [PDESoft 2014](http://pdesoft.uni-hd.de/) in Heidelberg
* [User Meeting 2013](2013-09-usermeeting) in Aachen
* [PDESoft 2012](http://pdesoft2012.uni-muenster.de/) in Münster
* [User Meeting 2012](2012-06-usermeeting) in Münster (cancelled)
* [User Meeting 2010](2010-10-usermeeting) in Stuttgart

## Developer Meetings

* [Developer Meeting 2017](2017-03-devmeeting) in Heidelberg
* [Developer Meeting 2015](2015-09-devmeeting) in Heidelberg
* [Developer Meeting 2014](2014-09-devmeeting) in Berlin
* [Developer Meeting 2013](2013-09-devmeeting) in Aachen
* [Developer Meeting 2012](2012-06-devmeeting) in Münster
* [Developer Meeting 2010](2010-11-devmeeting) in Münster
* [Developer Meeting 2009](2009-11-devmeeting)
* [Developer Meeting 2008](2008-02-27-devmeeting) in Berlin
* [Developer Meeting oct. 2005](2005-10-05-devmeeting) in Freiburg (aka Hüttenworkshop)
* [Developer Meeting july 2005](2005-07-11-devmeeting)
* [Developer Meeting jan. 2005](2005-01-21-devmeeting) in Heidelberg
* [Developer Meeting 2004](2004-10-07-devmeeting)

## Special Topic Meetings

* [GlobalFunctions 2013](2013-08-globalfunctions) in Münster
* [PDELab 2017](2017-03-pdelab) in Heidelberg
