+++
title = "License"
[menu.main]
parent = "about"
weight = 4
+++

### License
The DUNE library and headers are licensed under version 2 of the [GNU General Public License](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html), with a special exception for linking and compiling against DUNE, the so-called "runtime exception." The license is intended to be similar to the GNU Lesser General Public License, which by itself isn't suitable for a template library.

The exact wording of the exception reads as follows:

_As a special exception, you may use the DUNE library without restriction.  Specifically, if other files instantiate templates or use macros or inline functions from one or more of the DUNE source files, or you compile one or more of the DUNE source files and link them with other files to produce an executable, this does not by itself cause the resulting executable to be covered by the GNU General Public License.  This exception does not however invalidate any other reasons why the executable file might be covered by the GNU General Public License._

This license clones the one of the libstdc++ library. For further implications please see the [license page of libstdc++](http://gcc.gnu.org/onlinedocs/libstdc++/faq.html#faq.license). For the complete text of the GNU GPL see the COPYING file in this directory.
