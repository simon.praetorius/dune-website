+++
# This is the archetype for a new news item. No special keys needed,
# as all items will automatically be added the "news" type and the
# current date.
date="2016-07-25"
title = "DUNE/Fem Course at University of Stuttgart (Sep 26-30, 2016)"
+++

A one week Dune and Dune::Fem school will be
hosted at the IANS/University of Stuttgart.
The intended audience of the course are M.Sc. and PhD students.

The course will give an introduction to the Dune core modules including
the Dune grid interface. It will then proceed to the
discretization of PDEs with the Dune::Fem toolbox. Further
topics include the simulation of stationary and transient problems,
including essentials of mesh-adaptivity and parallel computing.

**Dates:**
September 26 - 30, 2016 (Mo - Fr)

**Registration:**
Participants should register until end of August (see course page below).

**Fees:**
Workshop fees are 50 Euro
in order to cover expenses for coffee breaks and course material.

**Course venue:**
Institute for Applied Analysis and Numerical Simulation (IANS)
Pfaffenwaldring 57, 70569 Stuttgart, Germany.

Further information and a registration form can be found at the course page
http://www.ians.uni-stuttgart.de/events/dune-fem-school-2016.
