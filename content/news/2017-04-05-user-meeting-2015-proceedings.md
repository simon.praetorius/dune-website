+++
date = "2017-04-05"
title = "Proceedings of the third DUNE User Meeting published"
+++

We are happy to announce that the proceedings of the
third <span style="font-variant: small-caps">Dune</span> user meeting,
held in 2015, have been published in
[archives of numerical software](http://journals.ub.uni-heidelberg.de/index.php/ans). You can find all ten papers in the
[current issue](http://journals.ub.uni-heidelberg.de/index.php/ans/issue/view/3214) of the magazine.

Please note that we are currently working on a print edition and will let you know once it is available.

We would like to thank all authors and referees for their valuable work.
  
