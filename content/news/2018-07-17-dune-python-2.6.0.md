+++
date = "2018-07-17"
title = "dune-python 2.6.0 released"
+++

The first release of the new [dune-python][] has finally
arrived!

This module provides the general infrastructure for exporting realizations
of statically polymorphic interfaces to Python based on just-in-time
compilation as well as bindings for the central interfaces of the
<span style="font-variant: small-caps">Dune</span> [core modules][].
This makes it possible to use Python to perform pre and post processing steps
and to test new algorithms before transferring the code to C++. Adding
Python bindings for additional modules and importing
free standing C++ template functions is straightforward.

In this first release, we focus on the grid interface.
Some grid implementations residing in external
<span style="font-variant: small-caps">Dune</span> modules, like
[ALUGrid][dune-alugrid], [SPGrid][dune-spgrid], and the new
[PolygonGrid][dune-polygongrid], also provide Python bindings in the presence
of dune-python.

This release is accompanied by a recently finished
[article][dune-python paper] describing the mechanisms behind dune-python and
providing a detailed introduction into its use.

You can get the code by cloning the [git repository][dune-python git] and
checking out the `v2.6.0` tag or by downloading the [source archive][archive].

[dune-python]: /modules/dune-python
[dune-python git]: https://gitlab.dune-project.org/staging/dune-python
[dune-python paper]: https://arxiv.org/abs/1807.05252
[archive]: /download/2.6.0/dune-python-2.6.0.tar.gz
[core modules]: /groups/core
[dune-alugrid]: /modules/dune-alugrid
[dune-spgrid]: /modules/dune-spgrid
[dune-polygongrid]: /modules/dune-polygongrid
