+++
date = "2018-06-28"
title = "dune-typetree 2.6.0 released"
+++

A new version of the [dune-typetree][] module has been released.
The new _dune-typetree_ 2.6.0 release is compatible
with the 2.6 release of the Dune [core modules][].

You can get the code by cloning the [git repository][dune-typetree git] or
downloading the [source archive][archive]. The 2.6.0 release can be obtained by
checking out the `v2.6.0` tag in this repository via git. For changes in this
version please refer to the [CHANGELOG.md][] file contained in the repository.

[core modules]: https://dune-project.org/groups/core
[dune-typetree]: https://dune-project.org/modules/dune-typetree
[dune-typetree git]: https://gitlab.dune-project.org/staging/dune-typetree
[archive]: https://gitlab.dune-project.org/staging/dune-typetree/-/archive/v2.6.0/dune-typetree-v2.6.0.tar.bz2
[CHANGELOG.md]: https://gitlab.dune-project.org/staging/dune-typetree/blob/v2.6.0/CHANGELOG.md

