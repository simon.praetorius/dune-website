+++
date = "2018-03-28"
title = "Dune 2.6.0 Released"
+++

<span style="font-variant: small-caps">Dune</span> 2.6.0 has finally been
released.  You can [download the tarballs],
checkout the `v2.6.0` tag via Git, or hopefully soon get prebuilt packages from
Debian unstable.

Included in the release are the [core modules][] (dune-common,
dune-geometry, dune-grid, dune-grid-howto, dune-istl, dune-localfunctions)
and at this point in time the dune-alugrid module. Others soon to follow.
Details on the changes can be found in the [release notes][].

  [download the tarballs]: /releases/2.6.0
  [core modules]: https://gitlab.dune-project.org/core/
  [release notes]: /releases/2.6.0#dune-2-6-release-notes
