+++
date = "2013-03-06"
title = "PDELab 1.1.0 Released"
+++

The PDELab developers are proud to announce the 1.1 release of PDELab. This release is mostly a bug-fix release, but it also contains a number of new features, most importantly a new boilerplate layer to reduce the amount of code required to get a standard PDELab program up and running. It has also been tested with the recent 2.2.1 release of the Dune core modules. An overview of the changes in this release and a list of any caveats to consider can be found in the [release notes](http://cgit.dune-project.org/repositories/dune-pdelab/plain/RELEASE_NOTES?h=releases/1.1).
 For further information please see the [PDELab homepage](http://dune-project.org/pdelab/).
