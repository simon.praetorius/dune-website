+++
date = "2012-12-21"
title = "DUNE PDELab Spring Course, Heidelberg (Germany), March 11-15, 2013"
+++

The Distributed and Unified Numerics Environment (DUNE) is a software framework for the numerical solution of partial differential equations with grid-based methods. Using generic programming techniques it strives for both high flexibility (efficiency of the programmer) and high performance (efficiency of the program). DUNE provides, among other things, a large variety of local mesh refinement techniques, a scalable parallel programming model, an ample collection of finite element methods and efficient linear solvers.

DUNE-PDELab is a powerful tool for implementing discretisations of partial-differential equations. It helps to substantially reduce the time to implement discretizations and solvers for (systems of) PDEs based on DUNE. It is not only suitable for rapid prototyping but also for building highly performant simulation software and is used by a variety of projects already.

This one week course will provide an introduction to the most important DUNE modules and the PDELab finite element framework based on DUNE. Topics covered are the solution of stationary and time-dependent problems, as well as local adaptivity, the use of parallel computers and the solution of non-linear PDE's and systems of PDE's.

At the end the attendees will have a solid knowledge of the simulation workflow from mesh generation and implementation of finite element and finite volume methods to visualization of the results. Successful participation requires knowledge of object-oriented programming using C++ including generic programming with templates. A solid background on numerical methods for the solution of PDEs is expected.

Registration deadline: Sunday February 24 2013
 Dates: March 11 2013 - March 15 2013

Course venue: Interdisciplinary Center for Scientific Computing
 University of Heidelberg, Im Neuenheimer Feld 350/368
 69120 Heidelberg, Germany

Fee: The fee for this course is 200 EUR including course material, coffee and lunch breaks as well as course ice breaker on Monday and course dinner on Wednesday.

For registration and further information see <http://conan.iwr.uni-heidelberg.de/dune-workshop/index.html>
