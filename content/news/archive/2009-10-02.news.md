+++
date = "2009-10-02"
title = "Dune User Meeting Evaluation"
+++

Bernd Flemisch brought up the idea of a *Dune User Meeting*.

Bernd offers to organize such a meeting if there is enough interest in the Dune community. He setup a doodle poll to find out how much interest in a *Dune User Meeting* there actually is (<http://www.doodle.com/vzx9zbnz7aup8485>). The poll is open until Friday, 9th of October.

So please, Dune users out there, give your vote on this poll.
