+++
date = "2014-04-27"
title = "Dune-Fem-Howto"
+++

In addition to the new release of dune-fem we have also done a substantial overhaul of our dune-fem howto. The new module including html and pdf documentation with a step by step introduction on how to use dune-fem is now available for download from the [Dune-Fem homepage](http://dune.mathematik.uni-freiburg.de/).
