+++
date = "2005-07-11"
title = "9th DUNE Developer Meeting in Heidelberg"
+++

Juli 11. the 9th DUNE Developer Meeting took place. The main points we were discussing were

-   Feature freeze for the sequential Grid Interface.
-   Plans for a 1.0 release.
-   Index / Id concept for the Grids
-   Reference Elements.

Please refer to the [meeting minutes](/community/meetings/2005-07-11-devmeeting) for more details.
