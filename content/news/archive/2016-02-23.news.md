+++
date = "2016-02-23"
title = "Dune 2.4.1-rc2 Released"
+++

The second release candidate for the upcoming 2.4.1 maintenance release of the core modules is now available. You can download the tarballs from our [download](/releases/) page or checkout the v2.4.1-rc2 tag of the modules via git. This release candidate is almost identical to RC1, it mainly fixes a problem with the tarballs. Please go and test, and report the problems that you encounter.
