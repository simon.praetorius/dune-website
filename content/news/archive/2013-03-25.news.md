+++
date = "2013-03-25"
title = "Dune-SPGrid Released"
+++

A new, very efficent implementation of a structrued, parallel grid ([Dune-SPGrid](http://dune.mathematik.uni-freiburg.de/grids/dune-spgrid/)) has been released. The current version (2012.12) is compatible with version 2.2 of the Dune core modules. Compared to YaspGrid, it can communicate data on all codimensions and supports periodic boundary conditions through the same interface supported by AlbertaGrid and ALUGrid.
