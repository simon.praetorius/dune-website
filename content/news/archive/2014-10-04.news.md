+++
date = "2014-10-04"
title = "Results of the yearly developer meeting"
+++

On 22.-23.9.2014 twelve Dune developers gathered in Berlin for two days of discussion and planning of the future development of Dune. ~~The agenda of the meeting is available~~, and there are detailed [meeting minutes](/community/meetings/2014-09-devmeeting), thanks to Dominic.

Possibly the most relevant decision for users of Dune is in the release schedule. There will be a 2.4 release with various improvements and fixes before the end of the year. However, after that we will start to work on a 3.0 release which will include some more disruptive changes. The list includes

-   Dropping the AutoTools build system,
-   Removing the EntityPointer class, but making Entity objects copyable instead,
-   Introducing per-module namespaces.

See the [meeting minutes](/community/meetings/2014-09-devmeeting) for more details.
