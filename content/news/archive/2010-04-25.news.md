+++
date = "2010-04-25"
title = "Release of DUNE 2.0"
+++

<div markdown style="float:left; padding-right: 1em">
![](/img/dune20.jpg)
</div>
We are pleased to announce the release of Version 2.0 of the "Distributed and Unified Numerics Environment" (DUNE).

The release includes the three core modules dune-common, dune-grid, and dune-istl; and the tutorials dune-grid-howto and dune-grid-dev-howto. Together with these five packages we released the new package dune-localfunctions, which brings a new interface for finite element shape functions to Dune. This package will also become a core module. Besides many smaller things, the most notable changes are:

-   The dune-grid module now features reference elements for arbitrary dimensions. This required a new numbering of sub-entities of elements. **This change may affect your code in subtle ways if you depend on a particular numbering!**
-   A new grid implementation, the `GeometryGrid`, has been added. It wraps any other DUNE grid and replaces the geometry by a given other one. It can, for example, be used to transform a 2d Cartesian grid into a helix.
-   The `GridFactory` interface has been extended. It now allows file readers to pass data associated with a given entity to the user.

For further information, have a look at the [release notes](/releases/2.0.0/).
