+++
date = "2012-06-22"
title = "Dune in Debian Wheezy"
+++

For a while we have been working on packaging the Dune core modules for Debian. Now, a few days ago the first packages of Dune 2.2 have entered Debian Testing, which mean that they will be part of the next Debian release 'Wheezy'. If you have a Debian Testing installation you can install the packages right now with

```
sudo apt-get install libdune-grid-dev libdune-istl-dev libdune-localfunctions-dev
```

(which will also pull in the remaining packages automatically).

Many thanks go to Ansgar Burchardt, who did most of the packaging work.
