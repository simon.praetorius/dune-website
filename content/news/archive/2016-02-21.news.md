+++
date = "2016-02-21"
title = "Dune 2.4.1-rc1 Released"
+++

The first release candidate for the upcoming 2.4.1 maintenance release of the core modules is now available. You can download the tarballs from our [download](/releases/) page or checkout the v2.4.1-rc1 tag of the modules via git Please go and test, and report the problems that you encounter.
