+++
date = "2014-05-11"
title = "UG in Debian"
+++

The UG software has arrived in [Debian testing](http://packages.qa.debian.org/u/ug.html). It is configured to work with Dune, and provides the sequential grid manager. Thanks to Ansgar Burchardt who did the packaging work.
