+++
date = "2010-01-22"
title = "Module dune-solvers announced"
+++

The new module `dune-solvers` is now available for [download](http://numerik.mi.fu-berlin.de/dune/dune-solvers/). It contains a variety of algebraic solvers, all structured in a dynamically polymorphic class hierarchy. `dune-solvers` is not a core module but used and maintained by Carsten Gräser and Oliver Sander.
