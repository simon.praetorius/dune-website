+++
date = "2005-01-28"
title = "8th DUNE Developer Meeting in Heidelberg"
+++

January 21. the 8th DUNE Developer Meeting took place. The main points we were discussing were

-   the const policy,
-   the Id - concept,
-   the parallel grid interface.
-   the [meeting minutes](/community/meetings/2005-01-21-devmeeting).
