+++
date = "2012-10-04"
title = "New UG Patch File Completes Support for 2d Edge Communication"
+++

We have released a new version of the [UG patch file]($(ROOT)/external_libraries/install_ug.html). The new patch level number is "9". It completes the support for edge communication in 2d meshes. Also, it brings a few additional fixes and improvements. Consult the [changelog]($(ROOT)/external_libraries/install_ug.html) for details.
