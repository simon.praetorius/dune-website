+++
date = "2012-05-20"
title = "Dune 2.2beta1 Released"
+++

A first set of release candidates for the upcoming 2.2 release is now available. This includes all core modules except for dune-grid-howto, which is stuck due to a build-system [bug](http://www.dune-project.org/flyspray/index.php?do=details&task_id=1109). You can download the tarballs from our [download]($(ROOT)/download.html) page. Please go and test, and report the problems that you encounter.
