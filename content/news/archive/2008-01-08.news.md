+++
date = "2008-01-08"
title = "Release of dune-fem 0.9"
+++

The DUNE groups in Freiburg and Münster are pleased to announce the release of Version 0.9 of the [FEM Module](http://dune.mathematik.uni-freiburg.de/) of DUNE.

The dune-fem module is based on the dune-grid interface library, extending the grid interface by a number of discretization algorithms for solving non-linear systems of partial differential equations.

For further information and for downloads see the [dune-fem homepage](http://dune.mathematik.uni-freiburg.de/).
