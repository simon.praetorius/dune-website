+++
date = "2008-11-06"
title = "Release of DUNE 1.1.1"
+++

We are pleased to announce the release of Version 1.1.1 of the "Distributed and Unified Numerics Environment" (DUNE) [(download)](/releases/). This version is a bug fix version of the previous release 1.1.

DUNE is free software licensed under the GPL (version 2) with a so-called "runtime exception". This licence is similar to the one under which the libstdc++ libraries are distributed. Thus it is possible to use DUNE even in proprietary software.

The changes to from version 1.1 to version 1.1.1 are listed in the [release notes](/releases/1.1.1/). For detailed information a file containing the differences of the source file from each core module is available in the [download directory](/releases/1.1.1/).
