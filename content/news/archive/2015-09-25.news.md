+++
date = "2015-09-25"
title = "Dune 2.4 Released"
+++

We are pleased to announce the release of the new stable version 2.4.0 of the Dune core modules. This release contains various new features and bugfixes. The probably most prominent one is the change of the default build system from autotools to CMake. The autotools build system is now deprecated and will be removed in the next major release. For further improvements, deprecations, clean-ups, and known issues have a look at the [release notes](releases/2.4.0). You can download the tarballs from our [download](/releases/) page, checkout the v2.4.0 tag of the modules via git.
