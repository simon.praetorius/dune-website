+++
date = "2006-06-23"
title = "New ALUGrid version"
+++

Dune got updated to the latest version of ALUGrid.

The current version of Dune-ALUGrid will need the latest ALUGrid-library with version number 0.3 to compile. New implemented features are leaf and level communication for parallel runs.
