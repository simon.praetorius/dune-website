+++
date = "2015-06-08"
title = "3rd DUNE User Meeting, Heidelberg, September 28-29, 2015"
+++

We cordially invite all DUNE users to participate in the *3rd DUNE User Meeting 2015* to be held in Heidelberg in September 28-29, 2015. This is a meeting for our users to showcase how they are using DUNE, foster future collaborations, and present their needs to the DUNE community.

The format of the meeting will be rather informal and similar to the *Finite Element Fair*. This means that every participant will be able to give a presentation. Presentations should be prepared such that the time needed for presenting is held flexible, i.e. ranging from a few minutes of presentation time to about half an hour. At the start of the meeting we will divide the available presentation time between the speakers and choose the order of the speakers randomly.

Please indicate your interest and whether you would like to give a presentation in the [wiki](/community/meetings/2015-09-usermeeting). Although there is no fixed deadline, we would appreciate registering as soon as possible.

We will have the [DUNE Developer Meeting 2015](/community/meetings/2015-09-devmeeting) right after the user meeting.

The initiators of this meeting are Markus Blatt, Bernd Flemisch, and Christoph Grüninger.
