+++
date = "2014-03-18"
title = "tab completion for dunecontrol"
+++

If you are using bash and you install your dune-common module, you now have tab completion for the dunecontrol command in your shell. If you want to use the feature with your locally installed module, or within a source tree, you have to source the completion file. In the latter case you can just add the following code to your .bashrc

    . $DUNE_COMMON_ROOT/share/bash-completion/completions/dunecontrol

and add bin to your path variable

    export PATH=$PATH:$DUNE_COMMON_ROOT/bin

Now you can enjoy

    $ dunecontrol --o[TABTAB]
    --only=  --opts=

and friends.
