+++
date = "2007-05-18"
title = "UGGrid supports nonconforming grids"
+++

The UGGrid implementation of the grid interface now supports nonconforming meshes. These were provided by UG all along but not available through the grid interface. Now a new method `UGGrid::setClosureType()` allows to omit the green closure when doing mesh refinement. There are no restrictions on the number of hanging nodes you may have on a single edge.

Enjoy the new feature and let us know about any problems you run into.
