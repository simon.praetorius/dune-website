+++
date = "2014-07-30"
title = "DUNE-ALUGrid 2.3 released"
+++

A new version of the ALUGrid manager has been released. We have added many new features and improved the performance of the code. One important change is that it is no longer required to download and build a separate library to use ALUGrid. Instead we have combined both the implementation and the bindings into a new dune module. To use DUNE-ALUGrid simply download the [new module](https://gitlab.dune-project.org/extensions/dune-alugrid) from the user wiki. Then add dune-alugrid either under ` "Suggests" ` or ` "Depends" ` in your ` dune.module ` file. Now simply run ` dunecontrol ` and everything should work as before. There are only a few things to keep in mind:

-   We have removed the special grid types e.g. ALUGridConform. Instead the type of the grid is always of the form Dune::ALUGrid&lt; dimgrid, dimworld, eltype, refinetype, communicator &gt;. (where communicator has a default value). The values for ` eltype ` are ` cube,simplex ` and for ` refinetype> ` the values are ` conforming, nonconforming ` defined in the ` DUNE ` namespace. The ` GRIDTYPE ` defines can still be used as before.
-   The define ` HAVE_ALUGRID ` will not work correctly anymore. Since DUNE-ALUGrid is now a dune module the correct name for the define is ` HAVE_DUNE_ALUGRID. ` This can produce some problems...

Note that we will not be supporting older versions of ALUGrid anymore and the binding in dune-grid will be deprecated. New features and improvements include

-   Conforming refinement for the 3D simplex grid
-   Internal load balancing method based on a space filling curve, making DUNE-ALUGrid self contained also in parallel
-   Bindings for fully parallel partitioners using [zoltan.](http://www.cs.sandia.gov/Zoltan/) (metis is still supported as well)
-   Complete user control of the load balancing
-   Improved memory footprint

A detailed description of all the new features and some more details concerning the inner workings of DUNE-ALUGrid can be found in the paper [The DUNE-ALUGrid Module.](http://arxiv.org/abs/1407.6954) This is the paper we would now ask everyone to cite when using the DUNE-ALUGrid module.
