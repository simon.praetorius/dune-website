+++
date = "2005-10-07"
title = "1st DUNE Developer Cottage Workshop"
+++

During October 5th to 7th nearly all DUNE core Developers attended the first DUNE Developer Cottage Workshop. It took place at the "Schauinsland"-Cottage of the University Freiburg locate in the wonderful, though foggy, scenery of the black forest. The main goal of the meeting were

-   Discuss and start working on things pending for a 1.0 release.
-   Test all grids for DUNE compliance by testing several model problems on most grids.
-   Interface and treatment of boundary elements.
-   A better interface grid adaption and parallel grid methods.

Please refer to the [meeting minutes](/community/meetings/2005-10-05-devmeeting.txt) for more details.
