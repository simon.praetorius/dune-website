+++
date = "2006-12-15"
title = "New ALUGrid version (0.41)"
+++

Dune got updated to the latest version of ALUGrid, version 0.41.

There will be no changes until Dune-1.0, except bug fixes if any occur.
