+++
date = "2010-02-18"
title = "DUNE Course March 15-19, 2010 - deadline extended!"
+++

You can still register for the DUNE course until Monday, February 22.

IWR, University Heidelberg, Germany

<http://conan.iwr.uni-heidelberg.de/dune-workshop/>

The Distributed and Unified Numerics Environment (DUNE) is a software framework for the numerical solution of partial differential equations with grid-based methods. Using generic programming techniques it strives for both: high flexibility (efficiency of the programmer) and high performance (efficiency of the program). DUNE provides, among other things, a large variety of local mesh refinement techniques, a scalable parallel programming model, an ample collection of finite element methods and efficient linear solvers.

This one week course will provide an introduction to the most important DUNE modules. At the end the attendees will have a solid knowledge of the simulation workflow from mesh generation and implementation of finite element and finite volume methods to visualization of the results.

Successful participation requires knowledge of object-oriented programming using C++ including generic programming with templates (this knowledge will be brushed up on the first day of the course). A solid background on numerical methods for the solution of PDEs is expected.
