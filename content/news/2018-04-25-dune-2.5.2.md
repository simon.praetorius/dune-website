+++
date = "2018-04-25"
title = "DUNE 2.5.2 Released"
+++

The <span style="font-variant: small-caps">Dune</span> 2.5.2 maintenance
release is now available.  You can [download the tarballs] or
checkout the `v2.5.2` tag via Git.

Included in the release are the [core modules][] (dune-common,
dune-geometry, dune-grid, dune-grid-howto, dune-istl, dune-localfunctions)
and several modules from the [staging area][] (dune-functions, dune-typetree,
dune-uggrid).

From the <span style="font-variant: small-caps">Dune</span> core
modules, only _dune-common_, _dune-grid_ and _dune-localfunctions_ had
changes from 2.5.1; from the staging modules only _dune-uggrid_.

Please refer to the [release notes] for a more detailed listing of
what has changed in the new release.

<span style="font-variant: small-caps">Dune</span> 2.5.2 is a bug fix
release to keep <span style="font-variant: small-caps">Dune</span> 2.5
working for recent systems. It is advised to move to <span
style="font-variant: small-caps">Dune</span> 2.6.

  [download the tarballs]: /releases/2.5.2
  [core modules]: https://gitlab.dune-project.org/core/
  [staging area]: https://gitlab.dune-project.org/staging/
  [release notes]: /releases/2.5.2#dune-2-5-2-release-notes
