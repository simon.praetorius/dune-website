+++
date = "2017-08-23T18:42:37-05:00"
title = "Proceedings of the 2015 Dune user meeting have appeared"
+++

The proceedings of the 2015 Dune user meeting have appeared.  They contain ten
papers with highly interesting new results related to Dune.

![](/img/proceedings-2015-user-meeting-cover.jpg)

The proceedings are available online as a [special issue](http://journals.ub.uni-heidelberg.de/index.php/ans/issue/view/3214/showToc)
of the [Archive of Numerical Software](http://archnumsoft.org/).
You can also order printed copies through the
[University of Heidelberg library book store](http://books.ub.uni-heidelberg.de/heibooks/catalog/book/280).

Thanks go to the authors, the reviewers, Guido Kanschat, and the Heidelberg university library team.

