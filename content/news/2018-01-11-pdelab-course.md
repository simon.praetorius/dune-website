+++
date="2018-01-11"
title = "DUNE/PDELab Course at Heidelberg University (February 26 - March 2, 2018)"
+++

The Interdisciplinary Center for Scientific Computing at Heidelberg University will host its annual
DUNE and PDELab course on February 26 - March 2, 2018.

This one week course provides an introduction to the most important DUNE modules and especially to
DUNE-PDELab. At the end the attendees will have a solid knowledge of the simulation workflow from
mesh generation and implementation of finite element and finite volume methods to visualization of the
results. Topics covered are the solution of stationary and time-dependent problems, as well as local
adaptivity, the use of parallel computers and the solution of non-linear PDEs and systems of PDEs.

#### Dates

February 26, 2018 - March 2, 2018

#### Registration Deadline

Friday, February 16, 2018

For further information, see the [course homepage](https://conan.iwr.uni-heidelberg.de/events/dune-course_2018/).
