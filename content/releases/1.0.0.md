+++
date = "2007-12-20T16:03:43+01:00"
major_version = 1
minor_version = 0
modules = ["dune-common", "dune-istl", "dune-grid", "dune-grid-howto"]
patch_version = 0
title = "Dune 1.0.0"
version = "1.0.0"
signed = 0
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 1.0 - Release notes

## Changes to Previous Releases

Since this is the very first release there are no changes that we can list. But here is a short list of features that the DUNE 1.0 release provides.

*   **Six different Grid Implementations:**
    *   [SGrid:](../doc/doxygen/dune-grid-html/classDune_1_1SGrid.html) A structured grid in n space dimensions
    *   [YaspGrid:](../doc/doxygen/dune-grid-html/classDune_1_1YaspGrid.html) A structured parallel grid in n space dimensions
    *   [UGGrid:](../doc/doxygen/dune-grid-html/classDune_1_1UGGrid.html) The grid manager of the UG toolbox
    *   [AlbertaGrid:](../doc/doxygen/dune-grid-html/classDune_1_1AlbertaGrid.html) The grid manager of the Alberta toolbox
    *   [OneDGrid:](../doc/doxygen/dune-grid-html/classDune_1_1OneDGrid.html) A sequential locally adaptive grid in one space dimension
    *   [ALUSimplexGrid](../doc/doxygen/dune-grid-html/classDune_1_1ALUSimplexGrid.html), [ALUCubeGrid](../doc/doxygen/dune-grid-html/classDune_1_1ALUCubeGrid.html): A hexahedral, tetrahedral, and triangular grid with nonconforming refinement including parallelization with dynamic load-balancing.

*   **Linear Algebra**

    DUNE contains ISTL (the Iterative Solver Template Library) for powerful linear algebra. The main features are:

    *   Abstractions for block matrices (e.g. [compressed row storage](../doc/doxygen/dune-istl-html/classDune_1_1BCRSMatrix.html) and [block diagonal](../doc/doxygen/dune-istl-html/classDune_1_1BDMatrix.html)) and [block vectors](../doc/doxygen/dune-istl-html/classDune_1_1BlockVector.html)
    *   Block structure arbitrarily nestable
    *   High performance through generic programming
    *   Expression templates for BLAS1 routines
    *   Several standard solvers

*   **Quadrature Formulas**
    *   Quadrature rules for all common element types
    *   Rules for hypercubes up to order 19, for simplices up to order 12

*   **Input/Output**
    *   Visualization using [GRAPE](http://www.mathematik.uni-freiburg.de/IAM/Research/grape/GENERAL/)
    *   Output in Data Explorer format
    *   Reading and writing in the AmiraMesh format
    *   Reading grid files in the grid independent Dune grid format [DGF](../doc/doxygen/dune-grid-html/group__DuneGridFormatParser.html)
    *   Reading simplex grids through DGF constructed using the tools Tetgen/Triangle (http://tetgen.berlios.de, http://www.cs.cmu.edu/~quake/triangle.html)
    *   Subsampling of high-order functions
    *   Write grids and data in the format of the visualization toolkit (vtk)

## Tested Platforms

The DUNE core developers have tested DUNE on the following platforms.

*   Debian Linux Sarge and Etch, using mpich/lam and gcc (3.4, 4.0, 4.1)
*   Gentoo Linux, using openmpi/mpich and gcc (3.4, 4.0, 4.1)
*   HP XC Version 3.0/SFS 2.1 using gcc (3.4 ,4.0)
*   SuSE Linux 10.x using gcc

We have also gotten reports of successful installations on various flavours of Mac OSX.

For known bugs, please take a look at the [bug tracking system](../flyspray.html) and the documentation!
