+++
date = "2012-06-04T16:02:58+01:00"
major_version = 2
minor_version = 2
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-localfunctions", "dune-grid-howto", "dune-grid-dev-howto"]
patch_version = 0
title = "Dune 2.2.0"
version = "2.2.0"
signed = 0
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 2.2 - Release Notes

## Build system

*   The macro `DUNE_SYMLINK` has been removed. It had been deprecated since the 2.0 release. Its purpose was the creation of the symlink '`dune`' pointing from a modules top-level directory to itself (i.e., `dune-foo/dune --> dune-foo`). The link created too much confusion. In the current layout of a dune module, `dune-foo/dune` has to be an actual directory.
*   The `DUNE_DEPRECATED` macro has gotten a cousin, `DUNE_DEPRECATED_MSG`. It allows to append an information string which gets printed by the compiler along with the warning.
*   The resume feature of `dunecontrol` is now disabled by default, i.e., no resume file is written to the user's home directory. To enable this feature, you have to specify the name of the resume file in the variable `RESUME_FILE` of your options file.
*   The majority of m4 macros honors configure caching. To use this feature with `dunecontrol` add `--cache-file=/tmp/dune-config.cache` to your options file. Be aware that the file persists between `dunecontrol` runs. If you encounter any problems, you want to change the configuration, or to rerun all checks, the cache file should be removed manually.

## dune-common

*   The macro `DUNE_VERSION_NEWER` has lost its last argument '`revision`'. That argument was never supposed to be there and the macro did not actually use it. If you want to test for Dune module version numbers including the revision (third digit), please use the `DUNE_VERSION_NEWER_REV` macro. If you need to stay compatible with the 2.1 releases, you even have to use `DUNE_VERSION_NEWER_REV`. Notice that passing zero for the revision number is equivalent to calling `DUNE_VERSION_NEWER`.
*   `FieldVector::size` is now a method rather than an enum value. This makes `FieldVector` more compliant with the `stl` conventions for container classes. To ease the transition there is a special mechanism: For the time being, `FieldVector::size` remains an enum value, hence all your code compiles as before. However you get a compiler warning explaining the issue and suggesting to configure with `--enable-fieldvector-size-is-method`. If you do so, then `FieldVector::size` becomes a method, and you may have to go out and adjust your code. The configure flag is transitional and will be removed after dune 2.2 is out. We apologize for the inconvenience. There is no smoother way to do the transition, as you can't have `FieldVector::size` to be both an enum value and a method at the same time. If you do need the container size as an enum value use `FieldVector::dimension`.
*   A new macro `DUNE_UNUSED` has been introduced. It provides a portable replacement for the `__attribute__((unused))` compiler attribute. In other words, if your compiler whines about an unused variable, but for some reason you cannot remove the variable, then you can mark it with this attribute to make the compiler shut up. Thanks to Elias Pipping for the patch.

### Deprecated and removed features

*   The `ConfigParser` class, already deprecated in Dune 2.1, has been removed. Please use `ParameterTree` together with `ParameterTreeParser` instead.
*   The `Int2Type` mechanism, already deprecated in Dune 2.1, has been removed. Please use `integral_constant` instead.
*   The class `Dune::GeometryType` has been moved to dune-geometry. The header `<dune/common/geometrytype.hh>` is deprecated, please use `<dune/geometry/type.hh>` instead.
*   The `utility.hh` file, a backward-compatibility stub for `tupleutility.hh`, has been removed. Please use `tupleutility.hh` directly.
*   The `finitestack.hh` file has been deprecated. Please use `std::stack<Dune::ReservedVector>` instead of the `FiniteStack` class.
*   The `f5.m4` build system check has been removed. Apparently it was never actually used by anyone.
*   The `Generic_MPI_Datatype` class has been removed.
*   The methods `rbegin` and `rend` of the classes `DenseMatrix` and `DenseVector` have been removed.

## dune-geometry

New core module for mesh-independent and mostly geometric things. Most of its contents was taken out of dune-grid. The moved subsystems are listed below. Kudos to [Christoph Grüninger](http://www.hydrosys.uni-stuttgart.de/institut/mitarbeiter/person.php?name=1450) for doing much of the work.

*   `Dune::GeometryType` (previously dune-common)
*   Parts from the generic geometries (generic construction classes, corner mappings...). The `Dune::Geometry` class is part of the grid interface and therefore stays in dune-grid.
*   The generic reference elements (`Dune::GenericReferenceElements`) etc. (previously dune-grid.)
*   The quadrature rules `Dune::QuadratureRules` etc. (previously dune-grid.)

## dune-grid

*   The `AmiraMeshWriter` can now write 2d-in-3d simplex grids. These appear in Amira as `Surface` objects. Vertex data on such grids can also be written.
*   dune-grid now requires the new core module dune-geometry.
*   The `UGGrid` grid manager now supports communication on edges, but only if your grid is 3d. (Thanks to Bernd Flemisch and Alexander Schlaich for the implementation) **Update:** Now it also works for 2d grids if you stay on level zero.
*   Geometries are no longer returned as references but as objects, i.e., `Entity::geometry`, `Entity::geometryInFather`, `Intersection::geometry`, `Intersection::geometryInInside`, and `Intersection::geometryInOutside` now return an object instead of a reference. This geometry object is specified to remain valid until the grid is modified (or deleted).
    _Notice_: While most code will continue to work without change, **this might affect your code in a very subtle way,** if you store references (or pointers) to the geometry object. Such references will now refer to a temporary object, which will be deleted at the end of the corresponding name space (i.e., at the next closing brace).
*   Parallel VTK files produced by the `VTKWriter` used to get a prefix "`s_SIZE_:`" (`.pvtu`/`.pvtp` files) or "`s_SIZE_:p_RANK_:`" (`.vtu`/`.vtp` files). Since the colon "`:`" is unsupported on Windows and is annoying when using `scp`, the prefixes have been changed to "`s_SIZE_-`" and "`s_SIZE_-p_RANK_-`". You should check your post-processing scripts whether they are affected by this change.
*   The template parameter of the `VTKFunction` class is now a `GridView` (it used to be a `Grid`).

### Deprecated and removed features

All removed features have already been deprecated in dune-grid-2.1.

*   The method `BasicGeometry::normal` has been removed.
*   The `EntityPointer::compactify` method has been removed. The concept has been superseded by the `EntitySeed`.
*   The `UGGrid` that took a heap size as its sole argument has been removed. If you want to set the default UG heap size please use the method `UGGrid::setDefaultHeapSize`.
*   Two `read` methods of the `GmshReader` that had a grid as one of their arguments have been removed. Mesh readers are supposed to create the grid objects themselves.
*   The cast of the entity iterators (level/leaf/hierarchic) to `EntityPointer` has been removed. Although they provide a similar interface these iterators are not a `EntityPointer`s.
*   The file `referencetopologies.hh` has been marked as deprecated, and will be removed after the release of dune-grid 2.2\. It was an internal header not to be used by outside code, and has become obsolete.
*   The file `dynamicsubindexid.hh` has been marked as deprecated, and will be removed after the release of dune-grid 2.2.
*   The default implementation for EntitySeeds (as EntityPointers) has been removed. Each grid implementation now needs to implement EntitySeeds itself.

## dune-istl

*   `Matrix::transposedMult` is deprecated, use `Matrix::mtv`
*   Support for SuperLU 4.3.
*   Our wrapper of SuperLU now supports also `float`, `std::complex<float>`, and `std::complex<double>`. Unfortunately, SuperLU can only cope with one numeric type at a time. Therefore the user has to define `SUPERLU_NTYPE` to 0, 2, and 3 to get support for `float`, `std::complex<float>`, and `std::complex<double>`, respectively.
*   The `MatrixIndexSet` class now uses `size_t` for all variables holding sizes.
*   Precision of writeMatrixToMatlab is adjustable.
*   Added support for reading and writing in matrix market format.
*   Made parallel AMG scale to nearly 300.000 processors.
*   Added Parameter class to adjust AMG solver
*   `KAMG` implements aggregation AMG with Krylov cycle.

### Deprecated and removed features

*   The deprecated class `ISTLAllocator` has been removed. Default allocator is now `std::allocator`, and all new implementations should adhere to `std::allocator`'s interface.
*   The support for the external library SuperLU older than the version 3.0 from 2005 is deprecated.
*   The methods `rbegin` and `rend` of the classes `Matrix`, `BCRSMatrix`, `ScaledIdMatrix`, `DiagonalMatrix`, and `VariableBlockVector` have been removed.

## dune-localfunctions

*   The module now includes second-order Lagrangian shape functions on hexahedra and pyramids (Implementation by Bernd Flemisch).
*   For Q2 elements, the dimension of the reference cube can now be controlled with a template parameter. Unfortunately, the actual implementation is not dimension-independent yet, but 1d, 2d and 3d work. The old Q22D* classes are deprecated.
*   dune-localfunctions no longer requires dune-grid, but the new core module dune-geometry instead. dune-localfunctions still suggests dune-grid though, since it is used by some of the tests.
*   The module now includes the Brezzi-Douglas-Marini elements BDM1Q2 and BDM12D and the Raviart-Thomas elements RT1Q2D, RT12D, RT1Q3D and RT2Q2D (Thanks to Human Rezaijafari for the implementation).

## dune-grid-howto

## dune-grid-dev-howto

# Known Bugs

*   Our drop-in replacement of <tt>std::shared_ptr</tt> differs slightly from the official implementation. With <tt>Dune::shared_ptr</tt> only the destructor of the base class A will be called in the following example:

    <pre>struct A{...};
    struct B:public A{...};
    void voo()
    {
        shared_ptr<A> p = shared_ptr<A>(new B);
    }
    </pre>

    In contrast the <tt>shared_ptr</tt> in (std|std::tr1|boost) will call the destructor of <tt>B</tt> even if it is not virtual. We intend to resolve this issue only implicitly by eventually discarding our own reimplemention of <tt>shared_ptr</tt>:
*   The pseudo inverse used in the generic geometries might fail for nearly singular matrices. This is not really a problem unless the grid is nearly degenerate.

A list of all bugs can be found online in our bug-tracker [http://www.dune-project.org/flyspray/](http://www.dune-project.org/flyspray/).
