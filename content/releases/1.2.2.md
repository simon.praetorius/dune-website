+++
date = "2009-11-19T16:03:35+01:00"
major_version = 1
minor_version = 2
modules = ["dune-common", "dune-istl", "dune-grid"]
patch_version = 2
title = "Dune 1.2.2"
version = "1.2.2"
signed = 0
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 1.2.2 - Release Notes

**Warning:** Many things have been marked deprecated. However note that class deprecation is buggy in all versions of gcc prior to 4.3.

## dune-common

*   Set dimension to 3 in `GeometryType::makePyramid` and `GeometryType::makePrism`.
*   Add method `mtv` to `FieldMatrix`.
*   Fix `FieldMatrix<T,1,1>::determinant`.
*   Make duneproject query dependencies on second try.
*   Add support for `headercheck_IGNORE`.

## dune-grid

*   Add a configure-switch to enable iterators on index sets (by default, they are now disabled).
*   Fix typedef for `Entity` in iterators of higher codmension.
*   Correctly store codimension information in `GenericReferenceElement`.
*   Relax comparisons in `DGFParser` when removing duplicate vertices introduced by interval blocks. This allows to read finer meshes.
*   Fixed several problems in `SubSamplingVTKWriter` including triangulation of hypercubes and connectivity information in appended data sections.
*   Make default implementation of `IndexSet::contains` compile when iterators are absent from index set and implement the method for all grid implementations in dune-grid.
*   Fixed higher codimension and `Ghost_Partition` iterators for `AlbertaGrid`.
*   Corrected return type of `AlbertaGrid::globalRefine` to `void`.
*   Remove some warnings from `OneDGrid`.

## dune-istl

*   Fix `MPITraits` for `bigunsignedint`.
