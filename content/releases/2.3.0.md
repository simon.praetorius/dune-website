+++
date = "2014-02-10T16:02:52+01:00"
major_version = 2
minor_version = 3
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-localfunctions", "dune-grid-howto", "dune-grid-dev-howto"]
patch_version = 0
title = "Dune 2.3.0"
version = "2.3.0"
signed = 1
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 2.3.0 - Release Notes

*   All modules compile with g++ 4.7, 4.8 and Clang 3.1, 3.2, 3.3.

## buildsystem

*   The deprecated build system macro `DUNE_CHECK_ALL_M` has been removed. Please use `DUNE_CHECK_ALL` instead.
*   As an experimental feature CMake is supported as a replacement for the current buildsystem based on autotools. It is still undecided when DUNE will switch, how long the transition period will be or whether this switch will happen at all (see [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1000)).
    `dunecontrol` enables CMake when the flag `--use-cmake` is set, everything is built in the module's default build directory `build-cmake`.

## dune-common

*   The file `diagonalmatrix.hh` containing the class of the same name has been moved from `dune-istl` to `dune-common`, since it is now used from the `dune-geometry` module.
*   There is a new file `power.hh`, which collects various compile-time power methods.
*   The files `collectivecommunication.hh mpicollectivecommunication.hh mpiguard.hh mpihelper.hh mpitraits.hh` have been moved to the directory `dune/common/parallel` (see [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1055)).
*   The macro `DUNE_UNUSED_PARAMETER` marks intentional unused function parameters to get rid of compiler warnings.
*   Hash support is provided by `dune/common/hash.hh`.
*   An adapter `Dune::singleton` to turn a class into a singleton was added.
*   The macro `DUNE_CONSTEXPR` is defined as `constexpr`, if supported by the compiler, and empty otherwise.
*   The semantics of `Dune::PoolAllocator` have changed. It now has an internal state (the pool) and thus is not fully usable for non C++11 containers.
*   There is a new communicator (`VariableSizeCommunicator`) for the parallel index sets. Its communication methods are more adapted to the communication interface of dune-grid. It is usable for implementing these methods by using the mapping of entities to local and global indices and the partition information.

### Deprecated and removed features

*   `FieldVector::size` is now a method rather than an enum value. This makes `FieldVector` more compliant with the `stl` conventions for container classes. The configure flag `--enable-fieldvector-size-is-method` has been removed as its purpose was to ease the transition. If you need the container size as an enum value use `FieldVector::dimension`.
*   The class `Power_m_p` from the file `misc.hh` has been deprecated. Use the new class `StaticPower` from the new file `power.hh` instead.
*   The `geometrytype.hh` and `geometrytypeindex.hh` files, backward-compatibility headers for `<dune/geometry/type.hh>` and `<dune/geometry/typeindex.hh>`, have been removed. Please use `type.hh` and `typeindex.hh` directly.
*   The deprecated `finitestack.hh` file has been removed. Please use `std::stack<Dune::ReservedVector>` instead of the `FiniteStack` class.
*   The class `SelectType` (from `typetraits.hh`) has been deprecated. Please use the new class `conditional` instead, which does the same thing, but can be used like the corresponding one from the C++11 standard library.
*   Similarly, we have deprecated `TypeTraits::isPointer`, `TypeTraits::isReference`, `TypeTraits::PointeeType`, and `TypeTraits::ReferredType`. Please use `is_pointer`, `is_lvalue_reference`, `remove_pointer`, `remove_reference`, (respectively) instead.
*   The file `misc.hh` is deprecated. Most math-related stuff has moved to `math.hh`. The two methods `hasPrefix` and `hasSuffix` are now in the new header `stringutility.hh`. The methods `SQR` and `genFilename` disappear with no replacement.
*   Support for very old MPI implementations is now deprecated, specifically implementations without support for the MPI-2.1 standard. This should affect very few users because all common MPI packages have been compatible with that standard since 2009.

## dune-geometry

*   The method `ReferenceElement::geometry` returns the (affine) embedding of subentities into the reference element. It uses an AffineGeometry to interpolate the vertex positions of the subentity.
*   `AxisAlignedCubeGeometry`, a new implementation of the `Geometry` interface has been added. As expected, it implements the geometry for axis-aligned hypercubes of arbitrary dimension and codimension. Since such hypercubes have a lot of structure, the implementation is very fast.
*   Similarly, `AffineGeometry` implements the `Geometry` interface for affine geometries. In affine geometries the Jacobian of the map to the reference element is constant. Using this can result in more efficient code (thanks to Martin Nolte).
*   A reimplementation of the generic geometries, named `MultiLinearGeometry` has been added. It comes without a complicated TMP and abandons the use of dynamic polymorphism in the case of multiple geometry types. Despite these simplifications, it shows slightly better performance.
*   A new class `CompositeQuadratureRule` allows to build composite quadrature rules by refining a reference element and using a given other quadrature rule for each subelement. Due to some limitations of the virtual refinement mechanism, this currently only works for simplex elements.
*   Virtual refinement moved from dune-grid to dune-geometry.
*   Gauss-Lobatto quadrature rules added without external dependencies.
*   The enumeration values `Gauss`, `Jacobian_1_0`, and `Jacobian_2_0` used to denote 1d numerical quadrature rules have been replaced by the more informative names `GaussLegendre`, `GaussJacobi_1_0`, and `GaussJacobian_2_0`. For a transitional period, the old names are still available. **Note:** It is impossible to mark enum values as deprecated. After the next release the old names will disappear without a warning from the compiler.

### Deprecated and removed features

*   The methods `ReferenceElement::global` have been deprecated; use `ReferenceElement::geometry< codim>( i ).global` instead. The non-interface method `ReferenceElement::mapping`, which is superceded by the `global` method, has been deprecated, too.
*   The classes `GenericReferenceElement*` are renamed as `ReferenceElement*`. The old names are deprecated and will be removed after Dune 2.3.
*   Many functions and constructors using `topologyId` as an argument were removed which were deprecated in Dune 2.2\. In many cases a replacement, with `Dune::GeometryType` as an argument, has to be used.
*   The support for ALGLIB has been removed.
*   The method `volumeOuterNormal` on the `ReferenceElement` has been renamed to `integrationOuterNormal`. It has always actually returned the outer normal to a face whose length coincides with the integration element of the face. The old name is now deprecated and will be removed after Dune 2.3.
*   The class `MockGeometry` has been deprecated and will be removed in Dune 2.3. Use the new `MultiLinearGeometry` instead. In most cases, replacing MockGeometry by MultiLinearGeometry and adapting the includes will do.
*   The method `initializeTopology` of the class `ReferenceElement` has been deprecated and will disappear after the 2.3 release. There is no replacement, you are not supposed to call such a method at all.
*   The enumeration value `Invalid_Rule`, from the set of all possible one-dimensional rules, has been removed. There is no replacement.

## dune-grid

*   The `EntitySeed` now has a default constructor (see [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1221)). A state of an `EntitySeed` can now be checked using the method `isValid()`. After default construction the state is `not isValid()`.
*   As all other facades classes in the Dune grid interface. Grid implementors can access the mutable and const `EntitySeed` implementation via `Grid::getRealImplementation` (see [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1346)).
*   The type `Jacobian` exported by `Geometry` has been renamed to `JacobianInverseTransposed`. The old type is still there for backward compatibility.
*   The types `JacobianTransposed` and `JacobianInverseTransposed` are now set by the Geometry implementations. Previously they were hardwired to be `FieldMatrices` of the proper size. The change allows to use special purpose data types for, e.g., structured grids, resulting in quite a bit of speed-up. It means, though, that you cannot rely on the Jacobians to be FieldMatrices anymore. The precise interface requirements for these matrices is still being worked out.
*   `GeometryGrid` now uses the new `MultiLinearGeometry` instead of the generic geometry implementation.
*   There is a new class `GmshWriter`, which allows to write grids in the native format of the [gmsh](http://www.geuz.org/gmsh/) grid generator.
*   `EntitySeed` now has a default constructor, this is especially helpful if you want to store sets of `EntitySeed`s in an `std::vector`, or a similar container.
*   Grids have new methods `leafGridView` and `levelGridView`. These behave just like the corresponding `leafView` and `levelView` methods, respectively. Their advantage is that the names correspond to the method return types, which are `LeafGridView` and `LevelGridView`, respectively. The old methods will be deprecated eventually.
*   The persistent container interface was cleaned up, some methods are deprecated.
*   When the new flag `--enable-extra-utils` is set, the utility programs in `src/` are built.

### UGGrid

*   You need to have at least UG-3.9.1-patch9 now to use `UGGrid`.
*   If you have even UG-3.9.1-patch10 installed, `UGGrid` provides dynamic load-balancing for vertex data. Thanks to Bernd Flemisch.
*   `UGGrid` can do face and edge communication now. Thanks to Bernd Flemisch.
*   Parallel `UGGrid` implements edge ids now.

### Deprecated and removed features

*   The transition headers for the headers moved to dune-geometry were removed. Mostly quadrature rules and the generic reference element are affected.
*   Virtual refinement moved to dune-geometry and is deprecated in dune-grid.
*   The classes `ALUCubeGrid`, `ALUSimplexGrid`, and `ALUConformGrid` have been marked deprecated. Use instead the new class `ALUGrid` and specialize it with `cube` or `simplex` and `nonconforming` or `conforming` from `<dune/grid/alugrid/common/declaration.hh>`. [More details](https://lists.dune-project.org/pipermail/dune/2012-May/011298.html)
*   The struct `VTKOptions` has been removed, it was alreadey deprecated in Dune 2.1\. Use `VTK::ascii`, `VTK::base64`, `VTK::appendedraw` instead of `VTKOptions::ascii`, `VTKOptions::binary`, `VTKOptions::binaryappended`. Most of the issues will be resolved by executing

    <pre>for i in `find . -name \*.[ch][ch] | xargs grep VTKOption | cut --delimiter=":" -f 1`; do
     sed "s/VTKOptions/VTK/g" $i> tmp && mv tmp $i
    done
    for i in `find . -name \*.[ch][ch] | xargs grep binary | cut --delimiter=":" -f 1`; do
      sed "s/VTK::binary/VTK::base64/g" $i> tmp && mv tmp $i
    done
    for i in `find . -name \*.[ch][ch] | xargs grep binaryappended | cut --delimiter=":" -f 1`; do
     sed "s/VTK::binaryappended/VTK::appendedraw/g" $i> tmp && mv tmp $i
    done
             </pre>

*   The interface methods `ilevelbegin`/`ilevelend` and `ileafbegin`/`ileafend` on the `Entity` have been deprecated and will be removed after Dune 2.3\. Use the methods `ibegin`/`iend` on the corresponding grid view to obtain the intersection iterators.
*   The classes `HierarchicIterator`, `LeafIterator`, `LevelIterator`, and their header files have been deprecated and will be removed after Dune 2.3\. Use the `EntityIterator` interface instead.
*   The type `ctype` and the integer `dimensionworld` exported by the `Entity` facade class are deprecated now, and will be removed eventually. They give geometrical information, which is not supposed to be known to a topological object like the `Entity`. Please use `Geometry::ctype` and `Geometry::coorddimension` instead.
*   Two constructors of `YaspGrid` have been deprecated. These took the number of elements in a `FieldVector<int>` and the periodicity in a `FieldVector<bool>`. In their place, the corresponding constructors taking `array<int>` and `std::bitset` are to be used.

### Changes for maintainers of grid implementations

*   There is now an interface class `EntitySeed`, which forward calls to an implementation specific engine class. From now on users of grids only get this interface class, and not the engine directly. This brings EntitySeed in conformance with the rest of the grid interface. Implementors of grids may need to tweak their implementations a little.

## dune-istl

*   UMFPACK is a new linear solver backend. It is a direct solver which in some cases is faster up to factor of six compared to SuperLU.
*   `BCRSMatrix` has a new build mode called implicit. It may reduce memory usage and computation time. Some problematic but rare corner cases for setting up BCRS matrices are now forbitten for all build modes.
*   There is a new implementation of a sequential algebraic multigrid method (`FastAMG`), that better utilizes the memory bandwidth.
*   We have added a generalized conjugate gradient method (`GeneralizedPCGSolver`), that allows for using preconditioners whose behaviour might change from step to step.

### Deprecated and removed features

*   All transition headers which included parallel headers were removed. The original headers were moved to `dune/istl/parallel` or dune-common in Dune 2.1, use these instead.
*   `Matrix::transposedMult` was removed, it was deprecated in Dune 2.2\. Use `Matrix::mtv` instead.
*   The file `diagonalmatrix.hh` containing the class of the same name has been moved from `dune-istl` to `dune-common`, since it is now used from the `dune-geometry` module.

## dune-localfunctions

*   Support for Lagrangian shape functions of arbitrary order on cubes of dimension 1, 2, and 3 has been added (thanks to Matthias Wohlmuth).
*   Support for first-order dual mortar basis functions for simplices and cubes of any dimension has been added (thanks to Jonathan Youett).
*   Rannacher-Turek elements in 3d have been implemented (thanks to Christoph Gersbacher).
*   Support for Brezzi-Douglas-Marini elements of first order in 3d for cubes and of second order in 2d for simplices and cubes have been added (thanks to Human Rezaijafari). The interpolation operator is not or not properly implemented for these elements.
*   Raviart-Thomas elements have a new and more convenient naming scheme. Use the new headers `<dune/localfunctions/raviartthomas/raviartthomascube.hh>`, `<dune/localfunctions/raviartthomas/raviartthomassimplex.hh>` and the new classes `RaviartThomasCubeLocalFiniteElement` and `RaviartThomasSimplexLocalFiniteElement` instead of the deprecated ones.

### Deprecated and removed features

*   The header `<dune/localfunctions/lagrange/q22d.hh>`, deprecated in Dune 2.2, has been removed. Please use the more general `<dune/localfunctions/lagrange/qk.hh>` instead.
*   The header `<dune/localfunctions/lagrange/q2.hh>` has been deprecated. Again, please use the more general `<dune/localfunctions/lagrange/qk.hh>` instead.
*   The support for ALGLIB has been removed. Some of its features are provided by GMP. Some others, like Lobatto points for Lagrangian shape functions, are no longer provided.
*   The class `RannacherTurek2DLocalFiniteElement<D, R>` is deprecated and will be removed after Dune 2.3\. Please use `RannacherTurekLocalFiniteElement<D, R, 2>` instead.

## dune-grid-howto

## dune-grid-dev-howto

*   The whole module is deprecated and will be removed. `IdentityGrid` will be moved to dune-grid until the next release.

# Known Bugs

*   Our drop-in replacement of `std::shared_ptr` differs slightly from the official implementation. With `Dune::shared_ptr` only the destructor of the base class A will be called in the following example:

```
struct A{...};
struct B:public A{...};
void voo()
{
  shared_ptr<A> p = shared_ptr<A>(new B);
}
```

In contrast the `shared_ptr` in (std|std::tr1|boost) will call the destructor of `B` even if it is not virtual. We intend to resolve this issue only implicitly by eventually discarding our own reimplemention of `shared_ptr`:
*   The pseudo inverse used in the generic geometries might fail for nearly singular matrices. This is not really a problem unless the grid is nearly degenerate.
*   The parallel `UGGrid` may return wrong ids on very complex adaptively refined and load-balanced grids. The reason is that the Dune grid interface mandates that two entities on different levels have the same id if they are copies. Therefore the UGGrid id/subId methods look through the ancestry of a given entity to see if there are copies. However, UG does so-called vertical load-balancing, which means that the ancestry may be distributed across different processors. Since the current code does not take that into account, wrong ids will be returned in the presence of vertical load-balancing. This is a potentially severe issue, because users do not get error messages, only ids that are tacitly wrong.
*   `PoolAllocator` is a stateful allocator and therefore not usable with containers according to standard before C++11.
*   Building shared libraries with CMake might break if you are using external static libraries compiled without support for position independent code (g++ -fpic).

A list of all bugs can be found online in our bug-tracker [http://www.dune-project.org/flyspray/](http://www.dune-project.org/flyspray/).
