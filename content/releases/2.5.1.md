+++
date = "2017-07-18T11:42:00+02:00"
version = "2.5.1"
major_version = "2"
minor_version = "5"
patch_version = "1"
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-grid-howto", "dune-localfunctions"]
signed = 1
title = "Dune 2.5.1"
doxygen_branch = ["v2.5.1"]
doxygen_url = ["/doxygen/2.5.1"]
doxygen_name = ["Dune Core Modules"]
doxygen_version = ["2.5.1"]
[menu]
  [menu.main]
    parent = "releases"
    weight = -1
+++

# Download the Dune 2.5.1 staging module sources

- **dune-functions** \[ tarball: [dune-functions-2.5.1.tar.gz](/download/2.5.1/dune-functions-2.5.1.tar.gz), signature: [dune-functions-2.5.1.tar.gz.asc](/download/2.5.1/dune-functions-2.5.1.tar.gz.asc) \]
- **dune-typetree** \[ tarball: [dune-typetree-2.5.0.tar.gz](/download/2.5.0/dune-typetree-2.5.0.tar.gz), signature: [dune-typetree-2.5.0.tar.gz.asc](/download/2.5.0/dune-typetree-2.5.0.tar.gz.asc) \] (*Note: There is no 2.5.1 release*)
- **dune-uggrid** \[ tarball: [dune-uggrid-2.5.1.tar.gz](/download/2.5.1/dune-uggrid-2.5.1.tar.gz), signature: [dune-uggrid-2.5.1.tar.gz.asc](/download/2.5.1/dune-uggrid-2.5.1.tar.gz.asc) \]

# DUNE 2.5.1 - Release Notes

DUNE 2.5.1 is a bugfix release, but also includes some minor adjustments.
See below for a list of changes.

## General

### Dependencies

In order to build this version of DUNE you need at least the following software:

* CMake 2.8.12 or newer
* pkg-config
* A standard compliant C++ compiler supporting C++11 and the C++14 feature set of GCC 4.9.
  We support GCC 4.9 or newer and Clang 3.8 or newer. We try to stay compatible to ICC 15.1 and newer
  but this is not tested.

## dune-common

* `FMatrixHelp::eigenValues`: fix computation of eigenvalues in 2d ([dune-common!206][])
* `DenseMatrix`: add specialized version of `invert()` for 3x3 matrices ([dune-common!231][])
* `DenseVector`: use field type instead of vector component type for scalar
  multiplication ([dune-common!280][])
* support `ALLOW_CFLAGS_OVERWRITE` in addition to `ALLOW_CXXFLAGS_OVERWRITE` ([dune-common!233][])
* remove fallback implementation of `index_sequence` and `integer_sequence` ([dune-common!252][])
* Doxygen: install generated `*.js` files ([dune-common!232][]) and enable
  search engine ([dune-common!263][])
* update dunecontrol documentation ([dune-common!279][])
* update link to buildsystem documentation ([dune-common!217][])
* update mailing list addresses

  [dune-common!206]: https://gitlab.dune-project.org/core/dune-common/merge_requests/206
  [dune-common!231]: https://gitlab.dune-project.org/core/dune-common/merge_requests/231
  [dune-common!280]: https://gitlab.dune-project.org/core/dune-common/merge_requests/280
  [dune-common!233]: https://gitlab.dune-project.org/core/dune-common/merge_requests/233
  [dune-common!252]: https://gitlab.dune-project.org/core/dune-common/merge_requests/252
  [dune-common!232]: https://gitlab.dune-project.org/core/dune-common/merge_requests/232
  [dune-common!263]: https://gitlab.dune-project.org/core/dune-common/merge_requests/263
  [dune-common!279]: https://gitlab.dune-project.org/core/dune-common/merge_requests/279
  [dune-common!217]: https://gitlab.dune-project.org/core/dune-common/merge_requests/217

## dune-geometry

* use `Hybrid::forEach` instead of wrapper `ForLoop` ([dune-geometry!47][])
* update mailing list addresses

  [dune-geometry!47]: https://gitlab.dune-project.org/core/dune-geometry/merge_requests/47

## dune-grid

* include specialization of `BackupRestoreFacility` in `yaspgrid.hh` header
* UseUG.cmake: pass `OBJECT` to `add_dune_mpi_flags` ([dune-grid!149][])
* UGGrid: apply vertex reordering to boundary segment vertices ([dune-grid!147][])
* UGGrid: append to-be-visited sons at the end of the list ([dune-grid!153][])
* test-parallel-ug: return 77 when run in parallel with sequential dune-uggrid
  ([dune-grid!162][])
* dgf: make dgfgridfactory compile when
  `DUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS` is disabled
* gridinfo.hh: Use `subEntities(codim)` method instead of removed
  `count<codim>()` template method ([dune-grid!161][])
* `ParMetisGridPartitioner`: fix "temporary cannot bind to non-const ref" error
  ([dune-grid!187][])
* update mailing list addresses

  [dune-grid!149]: https://gitlab.dune-project.org/core/dune-grid/merge_requests/149
  [dune-grid!147]: https://gitlab.dune-project.org/core/dune-grid/merge_requests/147
  [dune-grid!153]: https://gitlab.dune-project.org/core/dune-grid/merge_requests/153
  [dune-grid!162]: https://gitlab.dune-project.org/core/dune-grid/merge_requests/162
  [dune-grid!161]: https://gitlab.dune-project.org/core/dune-grid/merge_requests/161
  [dune-grid!187]: https://gitlab.dune-project.org/core/dune-grid/merge_requests/187

## dune-grid-howto

* update mailing list addresses

## dune-istl

* use correct type for second template argument of `std::array` ([dune-istl!90][])
* update mailing list addresses

  [dune-istl!90]: https://gitlab.dune-project.org/core/dune-istl/merge_requests/90

## dune-localfunctions

* implement bdm2simplex2d interpolation ([dune-localfunctions!37][])
* test-orthonormal.cc: increase ε used for comparisons (for i386) ([dune-localfunctions!42][])
* update mailing list addresses

  [dune-localfunctions!37]: https://gitlab.dune-project.org/core/dune-localfunctions/merge_requests/37
  [dune-localfunctions!42]: https://gitlab.dune-project.org/core/dune-localfunctions/merge_requests/42

## dune-functions

* fix constructor of `FlatMultiIndex` ([dune-functions!50][])
* drop superfluous const qualifier on return type ([dune-functions!48][])
* update mailing list addresses

  [dune-functions!50]: https://gitlab.dune-project.org/staging/dune-functions/merge_requests/50
  [dune-functions!48]: https://gitlab.dune-project.org/staging/dune-functions/merge_requests/48

## dune-uggrid

* CMakeLists.txt: missing curly braces in include directories ([dune-uggrid!44][])
* update mailing list addresses

  [dune-uggrid!44]: https://gitlab.dune-project.org/staging/dune-uggrid/merge_requests/44
