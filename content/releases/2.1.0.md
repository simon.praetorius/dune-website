+++
date = "2011-05-07T16:03:00+01:00"
major_version = 2
minor_version = 1
modules = ["dune-common", "dune-istl", "dune-grid", "dune-localfunctions", "dune-grid-howto", "dune-grid-dev-howto"]
patch_version = 0
title = "Dune 2.1.0"
version = "2.1.0"
signed = 0
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 2.1 - Release Notes

## buildsystem

*   The name of dunecontrol's "resume" file, i.e., the file containing the modules that have not yet been successfully built, can now be specified in the options file (`RESUME_FILE=`_filename_).
*   When resuming a previous dunecontrol run, it is now possible to skip the first module (the one that caused the error) by passing `--skipfirst` on the command line.
*   The configure option `--enable-dunedevel` to activate the Dune-Developer-mode got removed. Additional checks are now enabled whenever compiling without NDEBUG.

## dune-common

*   The `ConfigParser` code has been split into two parts. The new class `ParameterTree` handles hierarchies of string parameters. The `ParameterTreeParser` class only handles the actual parsing. It behaves similarly to the old `ConfigParser`, but with a few additions:
    *   It interprets the string values `yes` and `true` (case insensitive) as the bool value `true`. Before, any non-number was interpreted as `false`.
    *   It can now read array-valued arguments.The old `ConfigParser` class is deprecated

### Deprecated or removed features

*   The file `utility.hh` has been renamed to `tupleutility.hh`. The old file is still there for backward compatibility, but it issues a compile-time warning.
*   The method `nSetBits` of the class `BitSetVector` has been removed. It was already deprecated in Dune 2.0\. Please use the method `count` instead.
*   The file `smartpointer.hh`, already deprecated in Dune 2.0, has been removed.
*   The file `helpertemplates.hh`, already deprecated in Dune 2.0, has been removed.
*   The file `fixedarray.hh`, already deprecated in Dune 2.0, has been removed.
*   The class `IsTrue`, already deprecated in Dune 2.0, has been removed.
*   The class `ConfigParser` is deprecated (see above).

## dune-grid

*   The GridViews now have a method `contains` that tells you whether a given entity of the grid is part of the grid view. This information was previously only available from the index sets.
*   There is a new helper class `StructuredGridFactory`, which allows to initialize unstructured grids with structured cube and simplex grids.
*   The gmsh reader can now also handle quadrilaterals, hexahedra, prisms, and pyramids.
*   The gmsh reader can now read one-dimensional grids.
*   The `subIndex` method in IndexSets is now also available for `Entity<cc>` with higher codimensions using `int cc` as template parameter. This implies that grid implementations must also provide `subIndex` as a template method instead of the previously possible non-template.
*   The nonstandard allocator used in the implementation of the generic geometries has been removed. All memory used internally is now allocated on the stack. This should fix a few issues that people have had with multi-threaded programs. It also means that a generic geometry is no longer a reference counting pointer. This might affect performance in a subtle way.

### Deprecated or removed features

*   The method `Intersection::boundaryId()` has been deprecated. Assignment of data to coarse grid boundary segments should now be done using the boundary index.
*   All code that still implemented the old (== pre-2.0) numbering of subentities has been removed. This includes the `ReferenceElement` class and related classes and the methods `subIndex<>`, `Geometry::operator[]` and a few others.
*   The methods `intersectionSelfLocal()`, `intersectionNeighborLocal()`, and `intersectionGlobal()` have been removed from the `Intersection`. Use `geometryInInside()`, `geometryInOutside()` and `geometry()` instead.
*   The capabilities `hasHangingNodes` and `IsUnstructured` have been removed.
*   Casting an iterator reference into an EntityPointer reference has been deprecated. An EntityPointer can still be copy constructed from an iterator, though.
*   The method `Grid::name()`, already deprecated in Dune 2.0, has been removed.
*   Copy constructor and assignment operator on the interface class `Intersection` are now protected, as they were never part of the official interface. To copy an `Intersection`, the `IntersectionIterator` should be copied instead.
*   Those `read` methods of the `GmshReader` that took a grid argument have been marked as deprecated. Reading grid files into existing grid objects was a UGGrid-specific quirk, which has become obsolete.
*   The `EntityPointer::compactify` method has been deprecated. The idea was stillborn and has been superseded by the `EntitySeed` concept.

## dune-istl

### Deprecated or removed features

*   The `ISTLAllocator` class has been deprecated. Default allocator for all ISTL classes is now `std::allocator`. This does not change the memory allocation behavior. However, allocators are now expected to conform to the `stl` interface.
*   The `BTDMatrix` class can now solve tridiagonal systems with non-scalar matrix blocks.

## dune-grid-howto

## dune-grid-dev-howto

## dune-localfunctions

# Known Bugs

*   Our drop-in replacement of `std::shared_ptr` differs slightly from the official implementation. With `Dune::shared_ptr` only the destructor of the base class A will be called in the following example:

```
struct A{...};
struct B:public A{...};
void voo()
{
  shared_ptr<A> p = shared_ptr<A>(new B);
}
```

In contrast the `shared_ptr` in (std|std::tr1|boost) will call the destructor of `B` even if it is not virtual. We intend to resolve this issue only implicitly by eventually discarding our own reimplemention of `shared_ptr`:
*   The pseudo inverse used in the generic geometries might fail for nearly singular matrices. This is not really a problem unless the grid is nearly degenerate.

A list of all bugs can be found online in our bug-tracker [http://www.dune-project.org/flyspray/](http://www.dune-project.org/flyspray/).
