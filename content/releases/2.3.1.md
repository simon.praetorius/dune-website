+++
major_version = 2
minor_version = 3
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-localfunctions", "dune-grid-howto", "dune-grid-dev-howto"]
patch_version = 1
title = "Dune 2.3.1"
version = "2.3.1"
signed = 1
doxygen_modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-localfunctions"]
doxygen_branch = ["v2.3.1"]
doxygen_url = ["/doxygen/2.3.1"]
doxygen_name = ["Dune Core Modules"]
doxygen_version = ["2.3.1"]
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 2.3.1 - Release Notes

*   All modules compile with GCC 4.9 and Clang 3.4.

## Build system

*   Support for CMake 3.0

## dune-common

*   Tab completion for dunecontrol
*   Use `std::chrono::high_resolution_clock` for the Timer class to improve scalability (see [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1454)).
*   Fix handling of ParMETIS flags.
*   Several fixes for PoolAllocator

## dune-geometry

*   Fix evaluation of Jacobian at tip of pyramid (see [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1465)).

## dune-grid

*   Several minor fixes and improvements if used with CMake.

## dune-istl

*   Improve support for SCOTCH, a ParMETIS replacement.
*   Fix bug in block tridiag solver.

## dune-localfunctions

No mentionable changes.

## dune-grid-howto

No mentionable changes.

## dune-grid-dev-howto

No mentionable changes.
