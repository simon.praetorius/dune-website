+++
# The name of the module.
module = "dune-python"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups).
# Currently recognized groups: "core", "disc", "grid", "external", "extension", "user"
group = ["extension"]

# List of modules that this module requires
requires = [ "dune-common", "dune-geometry", "dune-grid", "dune-istl", "dune-localfunctions" ]

# A string with maintainers to be shown in short description, if present.
maintainers = "Andreas Dedner, Martin Nolte"

# Main Git repository, uncomment if present
git = "https://gitlab.dune-project.org/staging/dune-python.git"

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "Python bindings for the DUNE core modules"
+++

### Introduction

This module provides Python bindings for all
<span style="font-variant: small-caps">Dune</span> core modules.

The aim of this module is to firstly provide the general infrastructure for
exporting realizations of statically polymorphic interfaces based on
just-in-time compilation and secondly to provide bindings for the central
interfaces of the <span style="font-variant: small-caps">Dune</span> core
modules.
This makes it possible to use Python to perform pre and post processing steps
and to test new algorithms before transferring the code to C++.

Staying true to the <span style="font-variant: small-caps">Dune</span>
philosophy, we only introduce a thin layer when passing objects into Python,
which can be removed when the object is passed back into a C++ algorithm.
This retains the efficiency of C++ algorithms and little maintenance cost is
incurred. Also the modular structure of
<span style="font-variant: small-caps">Dune</span>
is maintained so that adding bindings for additional modules is straightforward.

This module provides a good entry point for getting started with
<span style="font-variant: small-caps">Dune</span>
due to the flexibility of the Python environment. To facilitate the
transition to the C++ development all
the Python classes have a very similar interface to their C++ counterparts.
This makes rapid prototyping in Python and then transferring the resulting
code to C++ to increase efficiency straightforward. Furthermore, the Python
interfaces will be very familiar to experienced
<span style="font-variant: small-caps">Dune</span>
users and developers, leading to no issues when switching between C++ and
Python. Combining C++ code and Python code is straightforward since free
standing C++ functions can be easily called in a Python script and
in addition, vectorized versions of many interfaces allow for more efficient
code on the Python side.

A description of the mechanisms behind dune-python and a detailed introduction
to its use can be found in the accompanying article
[Dedner, Nolte. The DUNE-Python Module][dune-python paper].


### Download

<table>
<tr>
  <th>Version</th>
  <th>Source</th>
  <th>Signature</th>
</tr>
<tr>
  <td>2.6.0</td>
  <td><a href="/download/2.6.0/dune-python-2.6.0.tar.gz" download>dune-python-2.6.0.tar.gz</a></td>
  <td><a href="/download/2.6.0/dune-python-2.6.0.tar.gz.asc" download>dune-python-2.6.0.tar.gz.asc</a></td>
</tr>
</table>


[dune-python paper]: https://arxiv.org/abs/1807.05252
