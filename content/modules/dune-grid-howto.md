+++
git = "https://gitlab.dune-project.org/core/dune-grid-howto"
group = ["core", "tutorial"]
maintainers = []
module = "dune-grid-howto"
requires = ["dune-common", "dune-grid"]
title = "dune grid howto"
+++
