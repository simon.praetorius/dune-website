+++
# The name of the module.
module = "dune-testtools"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups).
# Currently recognized groups: "core", "disc", "grid", "external", "extension", "user"
group = ["extension"]

# List of modules that this module requires
requires = ["dune-common", "dune-python"]

# List of modules that this module suggests
suggests = ["dune-grid", "dune-alugrid"]

# A string with maintainers to be shown in short description, if present.
maintainers = "[Dominic Kempf](mailto:dominic.kempf@iwr.uni-heidelberg.de), [Timo Koch](timo.koch@iws.uni-stuttgart.de)"

# Main Git repository, uncomment if present
git = "https://gitlab.dune-project.org/quality/dune-testtools"

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "Tools for implementing system testing in Dune"
+++

# What is dune-testtools?

dune-testtools provides the following components:
* a domain specific language for feature modelling, which is
  naturally integrates into the workflow of numerical simulation.
* Tools to test whether a given PDE discretization does still
  yield the correct result without performance (or scalability)
  regressions.
* Integration of above tools into a CMake based build system.
* Extensions to the dune core modules to support the development
  of system tests.

# How to use dune-testtools

dune-testtools is written as a Dune module. You can put it as a
requirement into the dune.module file of your own module and
configure/build it through dunecontrol (see the documentation
of dune-common for details).

# Documentation

The documentation of dune-testtools is available [here](/sphinx/dune-testtools).

# Acknowledgments

The work by Timo Koch and Dominic Kempf is supported by the
ministry of science, research and arts of the federal state of
Baden-Württemberg (Ministerium für Wissenschaft, Forschung
und Kunst Baden-Württemberg).
