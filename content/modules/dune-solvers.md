+++
git = "https://git.imp.fu-berlin.de/agnumpde/dune-solvers"
group = ["extension"]
module = "dune-solvers"
requires = ["dune-common", "dune-grid", "dune-istl", "dune-localfunctions", "dune-matrix-vector"]
short = "A library for algebraic solvers for Dune"
suggests = ["dune-alugrid"]
title = "dune-solvers"
+++


### About

Dune-solvers is a Dune module containing various algebraic solvers. They are organized in a class hierarchy based on dynamic polymorphism. The algebraic data types from dune-istl are used for data storage. _dune-solvers_ depends on _dune-common_, _dune-istl_, _dune-grid_, and _dune-localfunctions_, where the latter two are only needed to assemble transfer operators for multigrid methods.

Some of the features are:

* Preconditioned CG solvers
* Linear multigrid
* Monotone multigrid, as described in R. Kornhuber, C. Gräser, "Multigrid Methods for Obstacle Problems", J Comp Math 27 (1), 2009, pp. 1-44
* Bindings for the ISTL AMG
* Bindings for IPOpt

### Download and Installation

_dune-solvers_ is a regular Dune module and can be installed the usual way.

You can download the current development version using anonymous git.

`git clone https://git.imp.fu-berlin.de/agnumpde/dune-solvers.git`

### Documentation

The module contains a class documentation created with doxygen.

### License

The dune-solvers library and headers are licensed under version 2 of the GNU General Public License (see below), with a special exception for linking and compiling against dune-solvers, the so-called "runtime exception." The license is intended to be similiar to the GNU Lesser General Public License, which by itself isn't suitable for a template library.

The exact wording of the exception reads as follows:

As a special exception, you may use the dune-solvers source files as part of a software library or application without restriction. Specifically, if other files instantiate templates or use macros or inline functions from one or more of the dune-solvers source files, or you compile one or more of the dune-solvers source files and link them with other files to produce an executable, this does not by itself cause the resulting executable to be covered by the GNU General Public License. This exception does not however invalidate any other reasons why the executable file might be covered by the GNU General Public License.

This licence clones the one of the libstc++ library. For further implications of this library please see their license page.

See the file included COPYING for full copying permissions.

### Mailing lists
_dune-solvers_ development and discussions happen mainly on the [dune-fufem mailing list](https://lists.spline.inf.fu-berlin.de/mailman/listinfo/dune-fufem).

### Maintainers
_dune-solvers_ has been mainly written by

1. Carsten Gräser
2. Oliver Sander

See the COPYING file contained in the source code for a complete list.

We welcome interest and contributions by additional developers.
