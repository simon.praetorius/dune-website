+++
# The name of the module.
module = "dune-acfem"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups).
# Currently recognized groups: "core", "disc", "grid", "external", "extension", "user"
group = ["user"]

# List of modules that this module requires
requires = ["dune-common", "dune-geometry","dune-grid","dune-fem"]

# List of modules that this module suggests
suggests = ["dune-alugrid", "dune-istl", "dune-localfunctions", "dune-spgrid"]

# A string with maintainers to be shown in short description, if present.
maintainers = "[The acfem team](/modules/dune-acfem#Team)"
title = "Adaptive Convenient Finite Elements"

# Main Git repository, uncomment if present
git = "https://gitlab.dune-project.org/dune-fem/dune-acfem"

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "An add-on to [DUNE-FEM](/modules/dune-fem) which aims at a convenient discretization of diffusion dominated problems with Adaptive Conforming Finite Elements."

# Doxygen documentation: Please specify the following keys to automatically build
# a doxygen documentation for this module. Note, that specifying the git key is
# necessary in this case. All of the keys should be lists of the same length.
# Each entry of the list specifies the parameter for a separate piece of documentation.
# You can use this feature to generate documentation for several branches.
#
# Specify the url, where to build the doxygen documentation
doxygen_url = ["/doxygen/dune-acfem/release-2.5", "/doxygen/dune-acfem/master"]
# Specify the branch from which to build, omit to build from master
doxygen_branch = ["releases/2.5", "master"]
# Specify to build a a joint documentation from the following list of modules,
# omit, to build a doxygen documentation only for this module. This list will
# be used for all documentations, no list of lists necessary...
#doxygen_modules = []
# Please specify the name of the doxygen documentation, that will be shown on the main page.
doxygen_name = ["DUNE-ACFEM", "DUNE-ACFEM"]
doxygen_version = ["2.5.1", "unstable"]
+++

An add-on to [DUNE-FEM](/modules/dune-fem) which aims at a convenient
discretization of diffusion dominated problems with Adaptive
Conforming Finite Elements. It is in spirit and origin based on the
[DUNE-FEM-HOWTO](/modules/dune-fem-howto). "Conforming" means "continuous" if the
non-discrete problem lives in an H1-context.

The idea was to factor out common code for the cG-FEM examples into a
(template-) library. ACFem adds the possibility to build complicated
models by forming algebraic expressions from a zoo of basic "atom"
models like

```cxx
myModel = laplaceModel + sinXFct*massModel + dirichletModel - rhsFct;
```

Care has been taken to do this in a way that the resulting code
exhibits a decent performance by eliminating redundant "zero
expressions" with template specializations. Still it is always
possible to simply code the entire PDE in question into one monolithic
model, or to augment the "model-zoo" by own models and combine those
"hand-coded" models with some of the existing ones. There is an automatically generated Doxygen documentation of [DUNE-ACFEM 2.5.1](/doxygen/dune-acfem/release-2.5) and of the master branch ([unstable](/doxygen/dune-acfem/master)).

With respect to the code base present in the Poisson- and
Heat-equation examples from the DUNE-FEM-HOWTO ACFem adds more
general boundary conditions and adaptive algorithms which resemble the
state of the Fem-Toolbox [Alberta](http://www.alberta-fem.de) in this
respect.

ACFem requires a compiler with decent C++-14 support. gcc >= 5 should work, as well as recent clang versions.

An example usage can be found in this [article](https://journals.ub.uni-heidelberg.de/index.php/ans/article/view/27475).

#### <a name="Team"></a> Team
[Institut für Angewandte Analysis und Numerische Simulation, Universität Stuttgart](http://www.ians.uni-stuttgart.de)

* [Martin Alkämper](http://www.ians.uni-stuttgart.de/nmh/alkaemper)
* [Claus-Justus Heine](http://www.ians.uni-stuttgart.de/nmh/heine)
* [Stephan Hilb](http://www.ians.uni-stuttgart.de/nmh/hilb)
* [Birane Kane](http://www.ians.uni-stuttgart.de/nmh/kane)
