+++
title = "Homebrew Installation"
[menu.main]
parent = "releases"
weight = 2
+++

To setup a basic Dune installation via Homebrew activate first the external repository `bempp/homebrew-bempp` via

```
brew tap bempp/homebrew-bempp
```

The available packages are dune-common, dune-geometry, dune-localfunctions, dune-grid and dune-alugrid. For example,
to obtain a basic installation of DUNE together with Alugrid just use

```
brew install dune-alugrid
```

All dependencies of dune-alugrid are pulled in automatically. This was successfully tested on Mac OS 10.10.3 with Xcode 6.3 and a fresh installation of Homebrew.
