+++
title = "Installation from Source on a Debian-based Linux System for Beginners"
[menu.main]
parent = "beginnersresources"
+++

Here we demonstrate how to install Dune from source on Ubuntu 16.04
LTS. This is the way to go if you want to have access to all of Dune's
source files (e.g. because you want to inspect or modify them), to
have more control over the build process (e.g. change compiler flags or
enable/disable certain libraries) and also get dune-alugrid. Consequently this way of
installation is more complex than installation from binary
packages. We will simplify life by providing shell scripts that do all
the work.

We assume that you are running a freshly installed Ubuntu 16.04 LTS
with no additional packages installed yet. In order to install and run
Dune you first need to install the following packages (for this to
work you need sudo rights):

```
sudo apt install git
sudo apt install cmake
sudo apt install mpich mpich-doc libmpich-dev
sudo apt install zlib1g zlib1g-dev
sudo apt install texlive
sudo apt install texlive-science
sudo apt install texlive-latex-extra
sudo apt install texlive-bibtex-extra
sudo apt install texlive-math-extra
sudo apt install texlive-fonts-extra
sudo apt install paraview
sudo apt install gmsh
```

Note: In fact, all the packages with texlive in the name
are not necessary if you do not want to build the documentation on
your computer. The Dune grid howto documentation is also available
[here](/doc/tutorials/grid-howto.pdf) instead.

In order to create a Dune source installation the following steps are
necessary:

1. Download and install any necessary external libraries such as UG
2. Download all dune modules from Dune's git server
3. Create an appropriate opts-file giving compiler name, compiler
options, location of the installed external libraries, etc.
4. Run Dune's build system in all the modules using `dunecontrol`.

In order to automate these steps we provide installer scripts. It
downloads UG as well as the following dune modules:

* dune-common
* dune-geometry
* dune-grid
* dune-alugrid
* dune-istl
* dune-localfunctions
* dune-grid-howto

Just check out the following repository

```
git clone http://conan2.iwr.uni-heidelberg.de/git/peter/dune-installer.git
```

and run the installer:

```
./dune-installer/core-2.4.1/installer.sh my-dune-dir
```

Here `my-dune-dir` is the name of a new directory where you want your
Dune installation in. This will download and install the UG grid
manager, download all Dune core modules and
create an opts-file and a build-script.

Now you can run the build-script in the newly created directory:

```
cd my-dune-dir
./buildmodules.sh
```

If this does not work for you then you need to edit the installer
script in `dune-installer/core-2.4.1/installer.sh` and ask some
expert (e.g on the mailing list).

If all went well, where are the compiled files? The build-script sets
up things for a so-called 'out-of-core' build.

```
cd release-build
```

gets you to a directory that mirrors all dune-modules but with the
created files in it. In every subdirectory you will find a link
`src_dir` that will lead to the corresponding source directory that
created these files.
