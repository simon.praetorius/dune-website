+++
title = "The View Concept"
+++
### The View Concept
Dune is designed to create a common efficient interface for a multitude of different grid packages. There are large differences in how the geometrical and topological information in a grid is created and stored. Common designs are:

* The elements are created and stored in dynamic memory. Pointers are used to represent elements (e.g. UG)
* Only a macro-grid is stored like above, elements on refined levels are created when needed and destroyed as soon as possible (e.g. Alberta)
* All elements are created on-the-fly (e.g. SGrid)

Depending on the problem each of those designs may perform better or worse than the others. Note that a persistent element structure does not generally exist so that references pose a problem.
The major idea in the Dune grid-interface is that access to the grid components (entities) is only possible via a view, i.e. the grid returns a datastructure which provides read-only information. To speed up execution even this information is (if possible) only generated on access. Ideally, the constructor of such a view is as lightweight as possible.

#### Entity
The `Entity` is the datastructure a grid implementation offers to grant a view on it's internal structures. It creates additional e.g. geometrical information upon access and may therefore be rather heavyweight in memory consumption. To avoid consistency problems and possibly expensive memory operations an `Entity` cannot be copied. The `Entity` datastructure does only exist inside a grid, access is possible either via a reference or an `EntityPointer`. As mentioned above the Entity is not necessarily persistent, therefore references to Entities cannot be copied either.

Usually the entities are accessed via iterators offered by the grid.

#### EntityPointer
An `EntityPointer` is a persistent data structure representing the identity on an Entity and can thus be used to be stored in lists, copied and compared. The grid implementation stores the minimal information in an `EntityPointer` needed to access or generate an `Entity` in constant time. Thus, it is assumed to be small in memory. Note though that this is only true as long as dereferenciation is not used.

#### Iterators
The grid iterators (`LevelIterator, LeafIterator, HierarchicIterator`) can be used to traverse a grid and gain a view on all entities. They also represent the identity of the current entity and are therefore subclassed from `EntityPointer`.

You may notice that the `IntersectionIterator` is a special case: the intersections it traverses are indeed between two entities. Both can be accessed via the `inside` and `outside` methods which return an `EntityPointer`.
