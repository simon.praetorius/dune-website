+++
title = "Installing GMSH"
+++

### Overview
Gmsh is an external software tool for creating, editing, and meshing Computer Aided Design (CAD) models. Meshes created and exported by Gmsh (mesh files with ending.msh) can be imported into a grid object in Dune via the `Dune::GmshReader` interface.

### Getting and installing Gmsh
Precompiled versions and the source files for Linux and Mac OS X are available from the [Gmsh homepage](http://geuz.org/gmsh).

* Download and extract the precompiled stable Gmsh release (currently 2.4.2) for your system. In the extracted Gmsh folder, the executable is located in the bin folder.
* To be able to import existing CAD models in common file formats (supported are BREP, STEP, ACIS, IGES and more), you need the development version of Gmsh's geometry modeling kernel, OpenCascade, installed. For Linux systems, the library is called libopencascade-dev.
* For Gmsh versions before 2.3.2, Gmsh and OpenCascade have to be built from scratch to allow the import of existing CAD models into Gmsh.
* The Gmsh version from the repositories of most Linux distributions are not capable of importing existing CAD models. However, you can create your own models from scratch and import the meshes into Dune as well.

### Notes
* For further documentation, have a look at the dune-grid-howto (see section [Tutorials](/doc/tutorial)) or the dune-pdelab-howto (section [External Modules](/modules/ext-modules)).
* Use unstructured, simplicial grids for Gmsh meshes (Gmsh internally handles all meshes as unstructured meshes and exports simplicial meshes only).
* `UGGrid, Alberta` and `ALUGrid` are ready to import Gmsh meshes.
