Licensing Information
=====================

Copyright
---------

For the list of copyright holders, please inspect the history of the Git repository
using a command line like
```
git log --format="%an <%ae>" [filename] | sort | uniq
```

In addition, this repository contains imported versions of several external projects:

- The [jQuery][1] JavaScript library in the directory `static/js`.
- The [Bootstrap][2] framework in the directories `static/{css,js}`.
- The [Font Awesome][3] font and CSS toolkit in the directory `static/font-awesome`.
- The [InstantClick][4] JavaScript library in the directory `static/js`.
- The [highlight.js][5] JavaScript source code highlighting library in the directory `static/js`.
- An imported version of the Hugo template as used by the [Hugo website](https://gohugo.io) in the directories `layouts/partials` and `static/{assets/font,css,fonts,js}`, which has been heavily modified and extended for this website.
- The [feed icon][10] for linking to rss feeds in `static/img/Feed-icon.svg` .


License
-------

Different parts of this repository are licensed under different terms, as laid out in [legal.md][9]. Most importantly, all of the content as found in the subdirectory `content/` is licensed under under a [Creative Commons Attribution-ShareAlike 4.0 International License][6]. Moreover, all source code that appears in this directory tree is additionally licensed under the [Unlicense][7].

For the licenses of the imported external projects, see [legal.md][9]. As the website template is based on the Apache-licensed Hugo website, all files in the subdirectory `layouts/` are licensed under the [Apache License, Version 2.0][8].

For the license terms of any scientific papers found in this repository, please contact the respective authors. For the license terms of any software packages in the directory `static/download`, please refer to the license information contained within that software distribution.


Links
-----

[1]: https://jquery.org
[2]: https://getbootstrap.com
[3]: https://fontawesome.io
[4]: http://instantclick.io
[5]: https://highlightjs.org
[6]: CC-BY-SA-4.0
[7]: UNLICENSE
[8]: APACHE-2.0
[9]: content/about/legal.md
[10]: https://commons.wikimedia.org/wiki/File:Feed-icon.svg
